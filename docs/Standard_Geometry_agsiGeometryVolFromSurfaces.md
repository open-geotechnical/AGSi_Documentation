


# AGSi Standard / Geometry

## agsiGeometryVolFromSurfaces

### Object description

An [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) object defines an element as the volumetric element (solid) between top and/or bottom surfaces. This is a linking object between model element and the source geometry for the surfaces, which will normally be [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) objects. Refer to [Geometry rules and conventions](./Standard_Geometry_Rules.md) for full details of how the volume should be interpreted.

The parent object of [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) is [agsiGeometry](./Standard_Geometry_agsiGeometry.md)

[agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) has associations (reference links) with the following objects:

- [agsiModelElement](./Standard_Model_agsiModelElement.md)
- [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)
- [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md)

[agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md) has the following attributes:


- [geometryID](#geometryid)
- [description](#description)
- [topGeometryID](#topgeometryid)
- [bottomGeometryID](#bottomgeometryid)
- [remarks](#remarks)


### Attributes

#### geometryID
Identifier that will be referenced by Model and Geometry objects as required. All identifiers used within the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``GeologyGCC``

#### description
Short description of geometry defined here  
*Type:* string  
*Example:* ``Gotham City Clay``

#### topGeometryID
Identifies the geometry for top surface, linking to an [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) or [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) object. Definition of both top and bottom surfaces is recommended to minimise the risk of error. Refer to [Geometry rules and conventions](./Standard_Geometry_Rules.md) for further details.  
*Type:* string (reference to geometryID for an [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) or [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) object)  
*Condition:* Required if bottomGeometryID not used  
*Example:* ``GeologyGCCTop``

#### bottomGeometryID
Identifies the geometry for bottom surface, linking to an [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) or [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) object. Definition of both top and bottom surfaces is recommended to minimise the risk of error. Refer to [Geometry rules and conventions](./Standard_Geometry_Rules.md) for further details.  
*Type:* string (reference to geometryID for an [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) or [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) object)  
*Condition:* Required if topGeometryID not used  
*Example:* ``GeologyGCCBase``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``
