# Guidance / Encoding

## Introduction to JSON

### What is JSON?

JSON (JavaScript Object Notation) is

* a lightweight data-interchange format
* easy for humans to read and write
* easy for machines to parse and generate
* based on a subset of the JavaScript Programming Language
* a text format that is completely language independent
* but it uses conventions that are familiar to programmers of the C-family of languages, including C, C++, C#, Java, JavaScript, Perl, Python, and many others
* very easy to parse in Javascript (for web apps), Python (and probably others)

JSON is now considered by many to be the best format for data transfer applications.

The current standard for JSON can be found at:

* <a href="https://www.iso.org/standard/71616.html" target="_blank">ISO/IEC 21778:2017 Information technology — The JSON data interchange syntax</a>
* <a href="https://tools.ietf.org/html/rfc8259" target="_blank">IETF RFC8259 (December 2017)</a>
* <a href="http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf" target="_blank">ECMA-404 (December 2017)</a>

!!! Note
    It is commonly reported that the above standards are consistent with each other.

A comprehensive guide can be found at
<a href="https://www.json.org/json-en.html" target="_blank">www.json.org</a>

### JSON objects and arrays

JSON Comprises two basic constructs:

**1.   Object = { Name : value } pair**

    :::JSON
    {"my property" : "its value" }

An object can have several properties:

    :::JSON
    { "my property" : "its value" ,    "another property" : "another value" }


**2.   Array = [ Ordered list of values ]**

This is an array:

    :::JSON
    [ "Item 1" , "Item 2" ,  "Item 3" ]

Arrays are one dimensional, but can be nested:

    :::JSON
    [ "Main list item 1", [ "Sublist item 1", "Sublist item 2" ], "Main list item 3" ]

Objects and arrays are defined using bracket notation, thus:

    :::JSON
    { "Objects use" : "curly brackets" }

    [ "Arrays use" : "square brackets" ]

Commas are used to separate object name : value pairs and list items.

Object **names** must be enclosed in double quotes.

    :::JSON
    { "Object name" : ... some value ... }

**Values** only require double quotes when they are  of the *string*
data type. Further detail on JSON data types is given below.

### JSON data types

JSON recognises the following data types:

* String: must be enclosed in "double quotes"
* Number: quotes not required
* Boolean: `true` or `false`
* null: `null`
* JSON object
* JSON array

Date/time and similar more specific data formats are not natively
supported by JSON.
Date/time data should be provided as string data, suitably formatted.
However, <a href="https://json-schema.org/" target="_blank">JSON Schema</a>,
which is a vocabulary that supports the documentation and
validation of JSON data, allows JSON string data to be
specified as date/time (or similar) formats, allowing them
to be validated as such.

### Characters requiring special treatment (escape)

The following characters are reserved characters that cannot be
used in JSON and must be properly 'escaped' to be used in strings.

* Newline to be replaced with **\n**
* Carriage return to be replaced with **\r**
* Tab to be replaced with **\t**
* Double quote to be replaced with **\\"**
* Backslash to be replaced with **\\\\**
* Backspace to be replaced with **\b**
* Form feed to be replaced with **\f**

Note that **single quotes should not be escaped**.

!!! Note
    This is similar to the list of escape characters in the C family of programming languages,
    including Python. An exception is single quotes which require escaping in Python but not in JSON.

### Formatting of JSON

JSON data can be written in one line, or spread over many lines, and white space may be added for readability.

Lines breaks and white space (not within object names or value strings) will be ignored in parsing.

Line breaks and a small number of reserved control characters must be escaped when these are required in strings (further details below).
