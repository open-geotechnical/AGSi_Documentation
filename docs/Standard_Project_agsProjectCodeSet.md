


# AGSi Standard / Project

## agsProjectCodeSet

### Object description

Sets of codes used by some parts of the schema, as listed in associations below. Not to be used for property and parameter codes in the Data group (use [agsiDataCode](./Standard_Data_agsiDataCode.md) object instead).  Sets organised according to what they are used for. Typically used for project specific ABBR codes inherited from AGS factual data. Inclusion of standard AGS ABBR codes used is optional (unless required by specification). The individual codes are contained within the embedded [agsProjectCodeSet](./Standard_Project_agsProjectCode.md) objects or found at the source specified.

The parent object of [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) is [agsProject](./Standard_Project_agsProject.md)

[agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) contains the following embedded child objects: 

- [agsProjectCodeSet](./Standard_Project_agsProjectCode.md)

[agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) has associations (reference links) with the following objects: 

- [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md)
- [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md)

[agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) has the following attributes:


- [codeSetID](#codesetid)
- [description](#description)
- [usedByObject](#usedbyobject)
- [usedByAttribute](#usedbyattribute)
- [sourceDescription](#sourcedescription)
- [sourceURI](#sourceuri)
- [concatenationAllow](#concatenationallow)
- [concatenationCharacter](#concatenationcharacter)
- [agsProjectCode](#agsprojectcode)
- [remarks](#remarks)


### Attributes

#### codeSetID
Internal identifier for this code set. Not currently referenced from other parts of the schema, but included for completeness as it may prove useful in some applications.  
*Type:* string (identifier)  
*Example:* ``CodeSetGeology``

#### description
Name or short description of the code set.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Geology codes``

#### usedByObject
Name of the AGSi object that references this code set.  
*Type:* string (name of relevant object in schema)  
*Condition:* Required  
*Example:* ``[agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md)``

#### usedByAttribute
Name of the attribute of the AGSi object that references this code set.  
*Type:* string (name of relevant attribute in schema)  
*Condition:* Required  
*Example:* ``geologyCode``

#### sourceDescription
Description of the source of the list of codes to be used for this set, if applicable. This could be a published source, a project reference or a file provided with this data set. Optional if the codes are provided as [agsProjectCodeSet](./Standard_Project_agsProjectCode.md) objects.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``BGS lexicon of geology codes``

#### sourceURI
Link to source of list of codes to be used for this set, if applicable. This could be a published source, link to a project reference, or a file provided with this data set. Optional if the codes are provided as [agsProjectCodeSet](./Standard_Project_agsProjectCode.md) objects.  
*Type:* string (uri)  
*Condition:* Recommended  
*Example:* ``https://webapps.bgs.ac.uk/lexicon/``

#### concatenationAllow
true if concatenation of any combination of codes in the list is permitted, e.g. composite exploratory hole types when using AGS ABBR codes. Assume false (not permitted) if attribute omitted.  
*Type:* boolean  
*Example:* ``false``

#### concatenationCharacter
Linking character(s) used for concatenation, if permitted. Input blank string if none.   
*Type:* string  
*Example:* ``+``

#### agsProjectCode
Array of embedded [agsProjectCodeSet](./Standard_Project_agsProjectCode.md) object(s)  
*Type:* array (agsProjectCode object(s))  


#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Same as AGS ABBR code used in GI Package A``

