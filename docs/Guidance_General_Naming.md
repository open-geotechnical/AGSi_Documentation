# AGSi Guidance / General

## Governance - Naming conventions

!!! Note
    Work in progress - copied from old page - need to go through this.


#**Object names** use PascalCase except that all are preceded by the prefix *ags* or *agsi* in lower case. Example: **agsiObjectName**

The object name prefix *agsi* is used for objects that are unique to the AGSi schema whereas *ags* is used for objects that are expected to be eventually incorporated in the AGS Common schema (as described above).

!!! note
    OGC GML standards use PascalCase but with a namespace prefix in lower case (separated by a colon), e.g. gsmlb:GeologicUnit. IFC has names starting with 'Ifc', e.g. IfcPile.*

**Attribute names** use camelCase. Example: *attributeName*

!!! note
    OGC GML standards use camelCase. IFC uses PascalCase.

Object and attribute names should be meaningful. Full words should generally be used, except that reasonable truncation or abbreviation may be used to avoid very long names.  Example: *valueProfileIndVarCodeID*

!!! note
    This is common convention. Applied in OGC GML standards and IFC.

Object names shall incorporate the object name of its top level parent, but should not include any further parent names.

Example: [agsiData](./Standard_Data_agsiData.md) (top level parent); [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) (2nd level child)


!!! note
    Including all parent names would lead to very long and cumbersome object names.

Where one object is embedded (nested) within another (as an attribute), the parent object attribute name shall generally be the child object name. Example: [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) has an attribute [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md), which will contain [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) objects.

!!! note
    Adopting this convention ensures that the object name will always appear in the data file (for nested Json encoding).

Where an attribute value is intended to be a reference to an identifier defined elsewhere in the schema, the source and target identifiers should be the same, and should end with 'ID'. Example: *[codeID](./Standard_Data_agsiDataParameterValue.md#codeid)* attribute in [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) references *[codeID](./Standard_Data_agsiDataCode.md#codeid)* attribute in [agsiDataCode](./Standard_Data_agsiDataCode.md) object.

An exception to the above is where clarification of the usage of the identifier is desirable, or required to avoid conflict. In such cases clarifying terms may be added to the source attribute name in front of the
target attribute name.

Example: *[valueProfileIndVarCodeID](./Standard_Data_agsiDataParameterValue.md#valueprofileindvarcodeid)* attribute of [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md)
references *[codeID](./Standard_Data_agsiDataCode.md#codeid)* attribute of [agsiDataCode](./Standard_Data_agsiDataCode.md) (clarifies and avoids
clash with *[codeID](./Standard_Data_agsiDataParameterValue.md#codeid)* attribute of [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md)
