


# AGSi Standard / Geometry

## agsiGeometryExpHole

### Object description

An [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) object provides the geometric data for a single exploratory hole (borehole, trial pit, CPT etc.). In addition, some limited additional metadata for the hole is included. Reference to an [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) object can be used to link additional data if required. Geological logging data is not included here: see [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md).

The parent object of [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) is [agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md)

[agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) has associations (reference links) with the following objects:

- [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md)

[agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) has the following attributes:


- [holeID](#holeid)
- [holeName](#holename)
- [topCoordinate](#topcoordinate)
- [verticalHoleDepth](#verticalholedepth)
- [profileCoordinates](#profilecoordinates)
- [holeType](#holetype)
- [investigationID](#investigationid)
- [investigation](#investigation)
- [date](#date)
- [propertySetID](#propertysetid)
- [remarks](#remarks)


### Attributes

#### holeID
Identifier that may be referenced by [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md). Not necessarily the same as the original hole ID (see holeName) as exploratory hole identifiers should be unique within a dataset. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``A/BH01``

#### holeName
Current name or ID of the exploratory hole for general use.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``BH01``

#### topCoordinate
Co-ordinates of the top of the exploratory hole, as a co-ordinate tuple.  
*Type:* array ([coordinate tuple](./Standard_General_Formats.md#coordinate-tuple))  
*Condition:* Required unless profileCoordinates used  
*Example:* ``[1275.5,2195.0,15.25]``

#### verticalHoleDepth
Final depth of exploratory hole for vertical holes only. For non-vertical or non-straight holes use profileCoordinates attribute instead  
*Type:* number  
*Condition:* Required unless profileCoordinates used  
*Example:* ``25``

#### profileCoordinates
Co-ordinates of the line of the exploratory hole, i.e. top, bottom and intermediate changes in direction if required. Input as ordered list of co-ordinate tuples starting at the top. Used for holes that are not vertical, or not straight. May be used for straight vertical holes as alternative to topCoordinate and verticalHoleDepth. Takes precedence over verticalHoleDepth if they are in conflict.  
*Type:* array (of [coordinate tuple](./Standard_General_Formats.md#coordinate-tuple)s)  
*Condition:* Required if topCoordinate and verticalHoleDepth not used  
*Example:* ``[[1275.5,2195.0,15.25], [1275.5,2195.0,-9.75]]``

#### holeType
Type of exploratory hole. Recommend using code from AGS format ABBR code list, e.g. CP+RC, with project specific codes defined using [agsProjectCodeSet](./Standard_Project_agsProjectCode.md). Alternatively, short description may be provided, e.g. cable percussion borehole with rotary follow on.  
*Type:* string ([code based on AGS ABBR recommended](./Standard_General_Formats.md#ags-abbr-codes--project-codes))  
*Condition:* Recommended  
*Example:* ``CP+RC``

#### investigationID
The investigation that this hole was undertaken for. Reference to the identifier for the corresponding [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) object, if used.  
*Type:* string (reference to investigationID of [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) object)  
*Condition:* Recommended if investigation attribute not used  
*Example:* ``GIPackageA``

#### investigation
The investigation that this hole was undertaken for. Simple text alternative to investigationID if [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) not used. Use of both attributes together is not recommended.  
*Type:* string  
*Condition:* Recommended if investigationID attribute not used  
*Example:* ``2018 GI Package A``

#### date
Date of exploration, conventionally the start date for holes that take more than one day.  
*Type:* string (date)  
*Example:* ``2018-05-23``

#### propertySetID
Reference to additional property data for this hole by reference to an [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) object. Could be used for additional hole metadata or for profiles of test results, e.g. SPT.  
*Type:* string (reference to propertySetID of [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) object)  
*Example:* ``PropertySPTABH01``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Original name on logs: BH1``
