# AGSi Codes and vocabularies

## Vocabulary

The following are terms recommended for use by the attributes identified. Refer to the attribute description in Standard for further details of intended usage.

!!! Note
    Use the menu on the right to quickly navigate to the relevant section.


### Used in Model group

#### Model type

Refer to Standard for
[definition of model type](./Standard_General_Definitions.md#type-of-model).
Used by
[*modelType*](./Standard_Model_agsiModel.md#modeltype)
attribute of
[agsiModel](./Standard_Model_agsiModel.md).

In addition to the following, any valid combination of
[model domain](#model-domain) and
[model category](#model-category) is permitted

Term | Description/remarks
--- | ---
`Geological model` | Refer to [definition in Standard](./Standard_General_Definitions.md#geological-model)
`Geotechnical design model` | Refer to [definition in Standard](./Standard_General_Definitions.md#geotechnical-design-model)
`Geological model` | Refer to [definition in Standard](./Standard_General_Definitions.md#geological-model)
`Hydrogeological model` | Refer to [definition in Standard](./Standard_General_Definitions.md#hydrogeological-model)


#### Model category

Refer to Standard for
[definition of model category](./Standard_General_Definitions.md#category-of-model).
Used by
[*category*](./Standard_Model_agsiModel.md#category)
attribute of
[agsiModel](./Standard_Model_agsiModel.md).


Term | Description/remarks
--- | ---
`Analytical` | Refer to [definition in Standard](./Standard_General_Definitions.md#analytical-model)
`Conceptual` | Refer to [definition in Standard](./Standard_General_Definitions.md#conceptual-model)
`Observational` | Refer to [definition in Standard](./Standard_General_Definitions.md#observational-model)



#### Model domain

Refer to Standard for
[definition of model domain](./Standard_General_Definitions.md#domain-of-model).
Used by
[*domain*](./Standard_Model_agsiModel.md#domain)
attribute of
[agsiModel](./Standard_Model_agsiModel.md).

Term | Description/remarks
--- | ---
`Engineering geology` |
`Geoenvironmental` |
`Geology` |
`Geotechnical` |
`Hydrogeology` |


#### Model element type

Used by
[*elementType*](./Standard_Model_agsiModelElement.md#elementtype)
attribute of
[agsiModelElement](./Standard_Model_agsiModelElement.md).

Term | Description/remarks
--- | ---
`Exploratory holes` |
`Exploratory hole column segments` |
`Fault` |
`Geological unit` | Typically used in observational models
`Geotechnical unit` | Typically used in analytical (design) models
`Groundwater` |
`Hydrogeological unit` |
`Lithological unit` | Use if only soil/rock description provided
`Piezometric surface` |
`Water` |
`Terrain` | Ground surface

### Used in Geometry group

#### Geometry type

Used by
[*geometryType*](./Standard_Geometry_agsiGeometryFromFile.md#geometrytype)
attribute of
[agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md).

Term | Description/remarks
--- | ---
`Area` |
`Line` |
`Surface` |
`Volume` | Volumetric / solid element


#### Geometry file format

Used by
[*fileFormat*](./Standard_Geometry_agsiGeometryFromFile.md#fileFormat)
attribute of
[Standard_Geometry_agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md).

Term | Description/remarks
--- | ---
`DGN` | Bentley proprietary, e.g. Microstation
`DWG` | Autodesk proprietary, e.g. AutoCAD
`DXF` | AutoCAD DXF, interoperable
`IFC` | Industry Foundation Classes, ISO 16739
`LandXML` | Non-proprietary
`STEP` | STEP-File, ISO 10303
`STL` | 3D systems stereolithography / Standard tessellation language



#### Geometry column set type

Used by
[*columnType*](./Standard_Geometry_agsiGeometryColumnSet.md#columntype)
attribute of
[Standard_Geometry_agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md).

Term | Description/remarks
--- | ---
`Geology` |
`Lithology` | Use if only soil/rock descriptions provided
