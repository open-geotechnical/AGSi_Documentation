# AGSi Codes and vocabularies

## Introduction

The following pages provide:

* recommended codes for use by [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) and
[agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md)

* recommended vocabulary / terms for use by various attributes, where specified in the schema


!!! Note
    The following are first draft lists provided for the beta release of AGSi.
    AGS welcomes feedback including suggestions for new codes or vocabulary terms for items not yet covered.

The code lists and vocabularies will be version  controlled separately from the Standard, to allow additions to be made as required.  

Given that AGSi is still at beta stage with the lists provided herein expected to develop at rapid pace, the intended version control for the codes and vocabularies is not yet implemented.
