


# AGSi Standard / Model

## agsiModel

### Object description

An [agsiModel](./Standard_Model_agsiModel.md) object is the parent object for a single model. It contains general metadata for a model as well as the embedded element, boundary and subset objects that make up the model. There may be several models/agsiModel objects in an AGSi file/data set.  However, a single [agsiModel](./Standard_Model_agsiModel.md) object may itself incorporate different ways of representing the model information and supporting data, e.g. 3D volumes, cross sections and exploratory hole logs may all be incorporated into the same [agsiModel](./Standard_Model_agsiModel.md). These different representations may be (optionally) identified as subsets of the model using [agsiModelSubset](./Standard_Model_agsiModelSubset.md) objects. 

The parent object of [agsiModel](./Standard_Model_agsiModel.md) is [root](./Standard_Root_Intro.md)

[agsiModel](./Standard_Model_agsiModel.md) contains the following embedded child objects: 

- [agsiModelElement](./Standard_Model_agsiModelElement.md)
- [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md)
- [agsiModelSubset](./Standard_Model_agsiModelSubset.md)

[agsiModel](./Standard_Model_agsiModel.md) has associations (reference links) with the following objects: 

- [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md)
- [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md)

[agsiModel](./Standard_Model_agsiModel.md) has the following attributes:


- [modelID](#modelid)
- [modelName](#modelname)
- [description](#description)
- [coordSystemID](#coordsystemid)
- [modelType](#modeltype)
- [category](#category)
- [domain](#domain)
- [input](#input)
- [method](#method)
- [usage](#usage)
- [uncertainty](#uncertainty)
- [documentSetID](#documentsetid)
- [agsiModelElement](#agsimodelelement)
- [agsiModelBoundary](#agsimodelboundary)
- [agsiModelSubset](#agsimodelsubset)
- [remarks](#remarks)


### Attributes

#### modelID
Identifier, possibly a UUID. This is optional and is not referenced anywhere else in the schema, but it is beneficial to include this to help with data control and integrity, and some software/applications may require it.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``1fb599ab-c040-408d-aba0-85b18bb506c2``

#### modelName
Short name of model  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Sitewide geological model``

#### description
More verbose description of model, if required.  
*Type:* string  
*Example:* ``C999 Package X Sitewide geological model exported from SomeGeoModelSoftware. Incorporates 2018 GI data ``

#### coordSystemID
Reference to coordinate system applicable to this model (agsProjectCoordinateSystem object).  
*Type:* string (reference to systemID of  [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) object)  
*Example:* ``MetroXYZ``

#### modelType
[Type of model](./Standard_General_Definitions.md#type-of-model). Incorporates domain and category.  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md), or appropriate combination of domain and category terms)  
*Condition:* Recommended  
*Example:* ``Geological model``

#### category
[Category of model](./Standard_General_Definitions.md#category-of-model).  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Observational``

#### domain
[Domain of model](./Standard_General_Definitions.md#domain-of-model).  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Example:* ``Engineering geology``

#### input
Short description of input data used by model an/or cross reference to document describing this.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Input data described in GIR section 3.3.2``

#### method
Short description of method used to create model, including software used, and/or reference to the document where this is discussed.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``3D model created in SomeGeoModelSoftware. See GIR section 3.3.3 for details.``

#### usage
Short description of intended and/or permitted usage including limitations or restrictions. Strongly recommended.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Observational and interpolated geological profile. For reference and visualisation only. Not suitable for direct use in design. See GIR section 3.3.4 for details.``

#### uncertainty
Short statement discussing uncertainty with respect to the information presented in this model.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``The boundaries of the geological units presented in this model are a best estimate based on interpolation between exploratory holes, which in some cases are >100m apart. In addition, in some places the boundaries are based on interpretation of CPT results. Therefore the unit boundaries shown are subject to uncertainty, which increases with distance from the exploratory holes. Refer to GIR section 3.3.4 for more information. ``

#### documentSetID
Reference to documentation relating to model (reference to [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object)  
*Type:* string (reference to documentSetID of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object)  
*Example:* ``PackageXGIRGDR``

#### agsiModelElement
Array of embedded [agsiModelElement](./Standard_Model_agsiModelElement.md) object(s)  
*Type:* array (agsiModelElement object(s))  


#### agsiModelBoundary
Single embedded [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) object  
*Type:* object (agsiModelBoundary object)  


#### agsiModelSubset
Array of embedded [agsiModelSubset](./Standard_Model_agsiModelSubset.md) object(s)  
*Type:* array (agsiModelSubset object(s))  


#### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

