# AGSi Guidance / General

## Introduction to the guidance

The Guidance provides advice on how to use AGSi, including recommendations for best practice.

The Guidance is not part of the Standard. In the event of conflict, the Standard takes precedence.

!!! Note
    The Guidance is work in progress and right now and is 'a little rough around the edges', e.g. some supporting images missing, some links may not work and some of the examples use a slightly out of date development version of the schema. Please bear with us while we tidy this up over the next few weeks.

Each page of the guidance will be subject to its own version control, which will be indicated on the relevant page.

!!! Note
    This version control system is not yet fully in place - it will be added as we update each page.
