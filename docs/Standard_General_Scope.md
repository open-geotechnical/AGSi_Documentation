# AGSi Standard / General

## Scope

### Scope of AGSi

AGSi is intended for use in the **ground domain** for the sharing or transfer of **interpreted data** including, but not limited to, **ground model**s.
The highlighted terms are defined in the following.

In an AGSi context a **domain** is a specified sphere of activity or knowledge.
The **ground domain** is a parent domain that includes:

* geotechnical engineering
* geology
* hydrogeology
* geoenvironmental
* other supporting disciplines

!!! Note
    AGSi is intended to serve all of the above, but the initial development work has focused on geotechnical engineering applications which are expected to provide the main use case.  

The data collected from ground investigation and monitoring is considered to be **factual data**. This data is then collated, studied and processed to create **interpreted data**. The outputs from interpretation may include the following:

* Succession of strata/units in the ground, including:
    * Plots and/or sections showing the strata/units encountered at each exploratory hole
    * Estimation of location of boundaries of strata/units between exploratory holes
    * Recommended location and extent of strata/units for analysis and design
    * Commentary on all of the above
* Test data (in situ or laboratory), including:
    * Statistical summaries of the factual data (range, mean, etc.)
    * Derived data, based on correlations
    * Plots of the data
    * Recommended parameters for analysis and design
    * Commentary on all of the above

An output from interpretation may be one or more **ground model**s, a digital visual representation of the ground identifying the different strata/units, often incorporating linked data. Ground models are typically 3D, but 2D cross sections and simple 1D profiles are also considered to be ground models for the purposes of AGSi.

There are potentially many different types of ground model that can be created, and there may be several models for each project. The
[Guidance](./Guidance_Model_Types.md)
provides further information on the different types of model. In particular, attention is drawn to the difference between
[observational models](./Standard_General_Definitions.md#observational-model)
and
[analytical (or design) models](./Standard_General_Definitions.md#analytical-model)

AGSi is intended to be used for the process and output from interpretation.

AGSi should not be used for the transfer of factual data from ground investigations or monitoring. The <a href="https://www.ags.org.uk/data-format/" target="_blank">AGS (factual) data format</a>
should be used for transfer of factual data. The relevant AGS (factual) data files should be identified as documents linked/referenced from AGSi.

It is sometimes useful to include selected field observations or test results from ground investigations in models. These will sometimes comprise factual data although in other cases the data may be interpreted having have undergone some processing. Examples include:

  * Record of strata/units encountered in exploratory holes, which may incorporate re-interpretation of the original data.
  * SPT N values, which may incorporate extrapolation and/or N60 correction

Inclusion of field observations or test results for visualisation within a model is permitted in AGSi. However, it is recommended that such
[complementary data](./Standard_General_Definitions.md#complementary-data)
only be provided where it adds value to the model.

Where parameters for analysis/design are shared using AGSi, the type of parameter to be shared should be determined and agreed by the user(s). It is recommended that parameters for analysis/design shared should be Eurocode 7 characteristic values or equivalent, not (factored) Eurocode 7 design values.

### Scope exclusions

The processes of interpretation and modelling and their outputs are outside the scope of this standard.

AGSi should not be used for the transfer of factual data from ground investigations or monitoring.

Details of construction works, whether existing or proposed (i.e. the design), are not covered by AGSi.

!!! Note
    Several existing open standard formats cover modelling of construction works, e.g.
    <a href="https://www.buildingsmart.org/standards/bsi-standards/industry-foundation-classes/" target="_blank">IFC (buildingSMART)</a> and
    <a href="https://www.ogc.org/docs/is" target="_blank">OGC standards</a>.

    AGSi is targeted at the modelling of the soil/rock in the ground, which is currently poorly served by the above standards.
    The intention is for AGSi to provide a standard for ground models that complements the existing open standards for the exchange of building and infrastructure models, improving interoperability in the BIM/GIS environment.   

Detailed modelling of man-made obstructions within the ground is outside the scope of AGSi. However, AGSi may be used for simplified models of obstructed ground, e.g. identification of areas/volumes with different risk profiles.

AGSi should be used to share ground parameters for analysis/design where assessment of these is required as part of the interpretation.
AGSi should not be used to share parameters that are outside of the scope of the interpretation, such as correlation factors that are to be determined by the designer.
