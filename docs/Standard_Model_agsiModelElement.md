


# AGSi Standard / Model

## agsiModelElement

### Object description

A model is made up of elements. These elements are defined by [agsiModelElement](./Standard_Model_agsiModelElement.md) objects. Each element will have geometry assigned to it, by reference to an object in the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) Group. Which class of object is referenced will depend on the form of geometry required. Elements may also have data (properties and parameters) associated with them.

The parent object of [agsiModelElement](./Standard_Model_agsiModelElement.md) is [agsiModel](./Standard_Model_agsiModel.md)

[agsiModelElement](./Standard_Model_agsiModelElement.md) has associations (reference links) with the following objects: 

- [agsiModelSubset](./Standard_Model_agsiModelSubset.md)
- [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md)
- [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md)
- any of the objects in the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group

[agsiModelElement](./Standard_Model_agsiModelElement.md) has the following attributes:


- [elementID](#elementid)
- [elementName](#elementname)
- [description](#description)
- [elementType](#elementtype)
- [subsetID](#subsetid)
- [geometryObject](#geometryobject)
- [geometryID](#geometryid)
- [areaLimitGeometryID](#arealimitgeometryid)
- [propertySetID](#propertysetid)
- [parameterSetID](#parametersetid)
- [colourRGB](#colourrgb)
- [remarks](#remarks)


### Attributes

#### elementID
Identifier, possibly a UUID. This is optional and is not referenced anywhere else in the schema, but it is beneficial to include this as some applications may require an identifier.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``GCC/W``

#### elementName
Name or short description of what this element represents.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Gotham City Clay, west zone``

#### description
More verbose description, as required. Usage may be determined by type of element, e.g. for a geological unit this could be used to describe typical lithology.  
*Type:* string  
*Example:* ``Stiff to very stiff slightly sandy blue/grey CLAY, with occasional claystone layers (typically <0.1m). Becoming very sandy towards base of unit.``

#### elementType
Type of element, i.e. what the element represents in general terms.  
*Type:* string (recommend using term from [vocabulary](./Codes_Vocab.md))  
*Condition:* Recommended  
*Example:* ``Geological unit``

#### subsetID
Identifies the subset that this element belongs to, if applicable, by reference to an [agsiModelSubset](./Standard_Model_agsiModelSubset.md) object.   
*Type:* string (reference to subsetID of [agsiModelSubset](./Standard_Model_agsiModelSubset.md) object)  
*Example:* ``GeologyUnitVolumes``

#### geometryObject
Object type (from Geometry group) referenced by geometryID attribute.  
*Type:* string (name of a valid [agsiGeometry](./Standard_Geometry_agsiGeometry.md) object)  
*Condition:* Recommended  
*Example:* ``[agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md)``

#### geometryID
Identifies the geometry for this element. The object type referenced will depend on the type of geometry, which should be compatible with that defined in geometryType.  
*Type:* string (reference to geometryID for an object in the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group)  
*Condition:* Required  
*Example:* ``GeologyGCC``

#### areaLimitGeometryID
If required, a limiting plan area for the element may be defined by reference to a closed polygon object. The polygon acts as a 'cookie cutter' so the element boundary will be curtailed to stay within the polygon. Geometry beyond the boundary is ignored. This allows a large element to be easily divided up into parts, e.g. to allow different properties or parameters to be reported for each part. Use with caution as it may not be supported by all software/applications. Confirm usage in specification.  
*Type:* string (reference to geometryID for an [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) or [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object)  
*Example:* ``GeometryAreaWest``

#### propertySetID
Link to property data specific to this model element, by reference to an [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) object.  
*Type:* string (reference to propertySetID of [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) object)  
*Example:* ``PropLC/W``

#### parameterSetID
Link to parameter data  specific to this model element, by reference to an [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) object.  
*Type:* string (reference to parameterSetID of [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) object)  
*Example:* ``ParamLC/W``

#### colourRGB
Preferred display colour (RGB hexadecimal)  
*Type:* string (RGB hex colour)  
*Example:* ``#c0c0c0``

#### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

