


# AGSi Standard / Data

## agsiDataPropertyValue

### Object description

Each [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) object provides the data for a single defined property. The property data conveyed may be a simple statistical summary,  a text based summary or a profile of data points. Refer to [Data general rules and conventions](./Standard_Data_Rules.md) for further details.

The parent object of [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) is [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md)

[agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) has associations (reference links) with the following objects: 

- [agsiDataCode](./Standard_Data_agsiDataCode.md)
- [agsiDataCase](./Standard_Data_agsiDataCase.md)

[agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) has the following attributes:


- [codeID](#codeid)
- [caseID](#caseid)
- [valueMin](#valuemin)
- [valueMax](#valuemax)
- [valueMean](#valuemean)
- [valueStdDev](#valuestddev)
- [valueCount](#valuecount)
- [valueText](#valuetext)
- [valueProfileIndVarCodeID](#valueprofileindvarcodeid)
- [valueProfile](#valueprofile)
- [remarks](#remarks)


### Attributes

#### codeID
Code that identifies the property. Codes should be defined in either the [agsiDataCode](./Standard_Data_agsiDataCode.md) object, or in the code dictionary defined in the [agsiData](./Standard_Data_agsiData.md) object.  
*Type:* string (reference to codeID of [agsiDataCode](./Standard_Data_agsiDataCode.md) object, or code dictionary)  
*Condition:* Required  
*Example:* ``TRIG_CU``

#### caseID
Code that identifies a specific usage or subdivision of the property. For example, an alternative statistical summary that excludes outliers. If the input is a code, this shall be defined in the [agsiDataCase](./Standard_Data_agsiDataCase.md) object. May be left blank, but the combination of codeID and caseID should be unique for each agsiDataProperty Value object.  
*Type:* string (reference to caseID of [agsiDataCase](./Standard_Data_agsiDataCase.md) object, or text)  
*Example:* ``ExclOutlier``

#### valueMin
Minimum value  
*Type:* number  
*Condition:* Recommended  
*Example:* ``78``

#### valueMax
Maximum value  
*Type:* number  
*Condition:* Recommended  
*Example:* ``345``

#### valueMean
Mean value  
*Type:* number  
*Example:* ``178.2``

#### valueStdDev
Standard deviation  
*Type:* number  
*Example:* ``36.4``

#### valueCount
Number of results  
*Type:* number  
*Condition:* Recommended  
*Example:* ``58``

#### valueText
Alternative text based summary, if required or preferred. May be needed when limiting values are not numeric (<0.001) or could be used to provide a list of results for very small data sets.  
*Type:* string  
*Example:* ``<0.01 to 12.57, mean 3.21, (16 results)``

#### valueProfileIndVarCodeID
Code that identifies the independent variable for a profile, i.e. what the property value varies against. Codes should be defined in either the [agsiDataCode](./Standard_Data_agsiDataCode.md) object, or in the code dictionary defined in the [agsiData](./Standard_Data_agsiData.md) object.  
*Type:* string (reference to codeID of [agsiDataCode](./Standard_Data_agsiDataCode.md) object, or code dictionary)  
*Condition:* Recommended  
*Example:* ``Elevation``

#### valueProfile
A profile of values as an ordered list of tuples of [independent variable value, parameter value]. Refer to [General /Data input formats](./Standard_General_Formats.md) for further information.  
*Type:* array ([profile](./Standard_General_Formats.md#profiles-or-arrays-of-coordinate-tuples))  
*Condition:* Recommended  
*Example:* ``[[15.5,60],[14.0,75],[12.5,105]]``

#### remarks
Additional remarks, if required  
*Type:* string  


