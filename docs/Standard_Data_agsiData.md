


# AGSi Standard / Data

## agsiData

### Object description

The [agsiData](./Standard_Data_agsiData.md) object is a container for all of the data for a file/data set. It is required in all cases where properties and/or parameters are provided. There is only one [agsiData](./Standard_Data_agsiData.md) object per [root](./Standard_Root_Intro.md) object. All other objects in the Data group are embedded within [agsiData](./Standard_Data_agsiData.md).

The parent object of [agsiData](./Standard_Data_agsiData.md) is [root](./Standard_Root_Intro.md)

[agsiData](./Standard_Data_agsiData.md) contains the following embedded child objects: 

- [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md)
- [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md)
- [agsiDataCase](./Standard_Data_agsiDataCase.md)
- [agsiDataCode](./Standard_Data_agsiDataCode.md)

[agsiData](./Standard_Data_agsiData.md) has the following attributes:


- [dataSetID](#datasetid)
- [description](#description)
- [codeDictionary](#codedictionary)
- [documentSetID](#documentsetid)
- [agsiDataParameterSet](#agsidataparameterset)
- [agsiDataPropertySet](#agsidatapropertyset)
- [agsiDataCase](#agsidatacase)
- [agsiDataCode](#agsidatacode)
- [remarks](#remarks)


### Attributes

#### dataSetID
Identifier, possibly a UUID. This is optional and is not referenced anywhere else in the schema, but it is beneficial to include this to help with data control and integrity.  
*Type:* string (identifier)  
*Example:* ``40b785d0-547c-41c5-9b36-3cd31126ecb0``

#### description
Similar usage to dataSetID but intended to be a more verbose description.  
*Type:* string  
*Example:* ``My Project Name GIR interpreted data``

#### codeDictionary
URI link to the dictionary/vocabulary used for the code list. Use of the  [AGSi code list](./Codes_Codelist.md) is recommended, but it can be changed to an alternate list, e.g. lists published by other agencies (UK or overseas) or major projects/clients. If this is populated, then use of [agsiDataCode](./Standard_Data_agsiDataCode.md) is optional.  
*Type:* string (uri)  
*Condition:* Required if [agsiDataCode](./Standard_Data_agsiDataCode.md) not used  
*Example:* ``https://ags-data-format-wg.gitlab.io/AGSi_Documentation/Codes_List/``

#### documentSetID
Reference to documents relevant to the properties and parameters described herein, such as a GIR, AGS (factual) data and/or GDR  
*Type:* string (reference to documentSetID of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object)  
*Condition:* Recommended  
*Example:* ``PackageXGIRGDR``

#### agsiDataParameterSet
Array of embedded [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) object(s)  
*Type:* array (agsiDataParameterSet object(s))  


#### agsiDataPropertySet
Array of embedded [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) object(s)  
*Type:* array (agsiDataPropertySet object(s))  


#### agsiDataCase
Array of embedded [agsiDataCase](./Standard_Data_agsiDataCase.md) object(s)  
*Type:* array (agsiDataCase object(s))  


#### agsiDataCode
Array of embedded [agsiDataCode](./Standard_Data_agsiDataCode.md) object(s)  
*Type:* array (agsiDataCode object(s))  
*Condition:* Required if codeDictionary not used  


#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``

