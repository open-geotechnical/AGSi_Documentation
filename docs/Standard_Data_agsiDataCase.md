


# AGSi Standard / Data

## agsiDataCase

### Object description

[agsiDataCase](./Standard_Data_agsiDataCase.md) is a dictionary of the  caseID codes used [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) and [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md), if applicable. Case can be used where the specific usage of a parameter needs to be defined, e.g. for a specific type of analysis. Case IDs may also be used to facilitate alternative reporting of properties, e.g. a statistical summary that ignores outliers.

The parent object of [agsiDataCase](./Standard_Data_agsiDataCase.md) is [agsiData](./Standard_Data_agsiData.md)

[agsiDataCase](./Standard_Data_agsiDataCase.md) has associations (reference links) with the following objects: 

- [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md)
- [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md)

[agsiDataCase](./Standard_Data_agsiDataCase.md) has the following attributes:


- [caseID](#caseid)
- [description](#description)
- [remarks](#remarks)


### Attributes

#### caseID
Identifying code for a defined case. Referenced by [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) and/or [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md)   
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``EC7Pile``

#### description
Short description of the case, e.g. use case for a parameter.  
*Type:* string  
*Condition:* Required  
*Example:* ``EC7 Pile design``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``

