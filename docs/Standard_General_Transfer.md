# AGSi Standard / General

## Transfer of AGSi data

The following is based on file based transfer of AGSi data, which is expected to be the predominant method of sharing AGSi.

!!! Note
    It is recognised that systems for sharing data that are not file based are emerging. No particular provisions for this are given herein, but it is recommended that the general principles given below should be applied where relevant. Further guidance may be added at a later date.

When AGSi data is transferred or shared, it should comprise:

- One AGSi data file (the **file**)
- Supporting files and documents, as required (**included files**)

The combination of the above is referred to in this Standard as the AGSi **file/data set**.

The AGSi *file* shall have the file extension **.json**.

Packaging of the file and included files within a ZIP file is recommended as this helps to ensure that the file/data set is transmitted and stored as one package.

Included files will typically comprise one or more of the following:

- **geometry** files, as referenced by
  [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)
  objects
- **documents**, as referenced by [agsProjectDocument](./Standard_Project_agsProjectDocument.md) objects

It is possible for an AGSi file to link to supporting files hosted in an external resource, such as a project document management system. In such cases provision of the supporting files and documents with the AGSi file is not mandatory. However, the following will need to be taken into account:

- version control of the supported files, i.e. are the linked files live documents/data that may change over time
- ability of users, and potential future users, to access to the host resource
- barrier to long term legacy use when host resource may not be accessible

It is recommended that, as a minimum, *geometry* files be transferred as *included files*, even if they are available via an external system. Inclusion of *documents* is optional, but recommended if there is any doubt about user access to the host external resource.

When *geometry* files are included with the AGSi *file*, the URI links provided in the AGSi file should be local relative links to the included files, not to the external resource location. For documents, the schema allows both local and external resource URI links to be provided.

It is recommended that the AGSi file is put in the root of the ZIP folder with all included files in subfolder named as shown below:

![Recommended folder structure](./images/General_Transfer_Folders.svg)

Usage of the subfolders is self-explanatory. Further division into subfolders below the above may be determined by the user(s).




.
