# AGSi Standard / Geometry

## Geometry object summary

The objects in the Geometry group represent different types of geometry, as described and illustrated below.

#### agsiGeometryFromFile

An [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object is a pointer to geometry data contained within an external file. This object also includes metadata describing the file being referenced.
This will generally be used for complex surface or volume geometry.
Refer to [Geometry rules and conventions](./Standard_Geometry_Rules.md#File formats for agsiGeometryFromFile)
for recommendations on file formats.

#### agsiGeometryLine

An [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine) object provides data for a polyline or polygon in 2 or 3 dimensional space. For a 2D line, the plane that the lines lies in will be determined by the context of the referencing object. Lines must comprise straight segments. Curves are not supported.

!!! Note
    The data structure used for [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine)
    is similar to that used for linestrings and polygons in
    <a href="https://geojson.org/" target="_blank">GeoJSON</a> and
    <a href="https://www.ogc.org/standards/sfa" target="_blank">OGC Simple Features</a>.

#### agsiGeometryLayer

An [agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md) object is a volumetric element bounded by two infinite horizontal planes at specified elevations. May be used for defining each element in a stratigraphical column (one dimensional) model. See
[Geometry rules and conventions](./Standard_Geometry_Rules.md) for interpretation.


#### agsiGeometryPlane

An [agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md) object
is an infinite horizontal plane at a specified elevation.			


#### agsiGeometryVolFromSurfaces

An [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md)
object defines an element as the volumetric element (solid) between top and/or bottom surfaces. This is a linking object between model element and the source geometry for the surfaces, which will normally be
[agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)
objects. Refer to [Geometry rules and conventions](./Standard_Geometry_Rules.md) for limitations and full details of how the volume shall be interpreted when using this object.

#### agsiGeometryAreaFromLines

An [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md)
object defines an element as the area between top and/or bottom lines.
It is similar to
[agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md)
but it represents 2D areas rather than volumes.
This will typically be used on cross sections or fence diagrams. This is a linking object between model element and the source geometry for the lines. Refer to [Geometry rules and conventions](./Standard_Geometry_Rules.md) for limitations and full details of how the area shall be interpreted when using this object.

#### agsiGeometryExpHoleSet & agsiGeometryExpHole

An [agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) object is a user defined set of exploratory holes (boreholes, trial pits, CPT etc.). A set may contain all holes, or if preferred the holes can be organised into several sets,  e.g. sets for different investigations, or different types of hole.

The data for the holes is included via the embedded [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md)
object which provides the geometric data for a single exploratory holes (boreholes, trial pits, CPT etc.). In addition, some limited additional metadata for the hole is included alongside an optional link to additional data defined in an
[agsDataPropertySet](./Standard_Data_agsiDataPropertySet.md) object.

Geologicical logging data is not included in these objects: see [agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md) and [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md).

#### agsiGeometryColumnSet & agsiGeometryColumn

An [agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md) object is set of stratigraphical column segments, typically used to represent geology in exploratory holes. Sets are expected to align with geological units, although additional or alternative subdivisions are permitted.

The data for the columns is included via the embedded [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md)
object which  provides the geometric data for a single column segment. In addition, essential data such as the geological description can be included. This object may be associated with an [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) object, but this is not essential.

For information on the exploratory holes themselves see [agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) and [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md).
