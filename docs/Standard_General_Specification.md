# AGSi Standard / General

## Specification of AGSi

This standard sets out a schema and rules that define AGSi. However, only a relatively small number of objects and attributes are mandatory, and in many cases the schema permits alternative methods of implementation. This standard does not provide a specification for the use of AGSi.
Where AGSi is to be provided, the end user (hereafter: [specifier](./Standard_General_Definitions.md#specifier)) shall provide a specification detailing their project specific requirements for the AGSi data set to be provided.

The specification should cover the following, as applicable:

* Version of schema to be used
* Codes and vocabularies: AGSi or a specified alternative
* Use of model subsets
* Use of limiting areas for model elements
* Use of model boundaries, or limitations, including
whether top boundary needs to be defined
* Permitted geometric form of model(s), e.g. 3D volumes or volumes from surfaces
* If volumes from surfaces used, whether top, bottom or both surfaces to be defined for each element
* Methodology for representing intermittent layers
* Permitted file formats for external geometry data
* Extent of [complementary data](./Standard_General_Definitions.md#complementary-data) to be provided
* If exploratory holes are required, whether holes are to be modelled in addition to the geological segments. Also, preference for use of sets.
* Extent of metadata to be provided, e.g. require all 'recommended' attributes to be completed where applicable, or provide a detailed specification
* Encoding requirements

The above are in addition to particular requirements relating to the interpretation methodology and the nature of the data/models to be provided. These are outside of the scope of this standard.

Where no specification has been provided, or where a data set has been created speculatively in anticipation of future sharing, the producer/modeller should carefully consider the issues listed above. The producer/modeller should document the approach taken, which should take into account, as far as reasonably practical, foreseeable interoperability issues.
