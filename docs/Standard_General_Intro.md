# AGSi Standard / General

The title of this standard is:

*Association of Geotechnical & Geoenvironmental Specialists transfer format for ground model and interpreted data (AGSi): Standard*

## Introduction

AGSi is the short name of a schema and transfer format for the exchange of ground model and interpreted data in the ground domain.

This standard defines the schema, rules, method of encoding and other requirements that constitute AGSi.

The online version of this standard, found by following the link provided on the AGS home page:
<a href="https://www.ags.org.uk/" target="_blank">https://www.ags.org.uk/</a>,
is the official revision controlled version.
It is not available in printed form or as a PDF.
Any printed or PDF copy shall be considered as uncontrolled.

### Contents of this standard

This standard is organised as follows:

* This **General** section which sets out general requirements and rules, including some common requirements that apply for all or many parts of the schema
* Sections describing each part (group) of the **schema**, each typically comprising an **overview**, **rules and conventions**  and pages providing full details for each object in the group (**object reference**).
* The **Encoding** section which describes rules and requirements for encoding applicable to file based transfer.  

This standard is supported by the following supplementary documentation:

* [JSON Schema](./Standard_Encoding_JSONSchema.md) file(s) for AGSi
* [Codes and vocabularies](./Codes_Intro.md) to be used in AGSi
* [Guidance](./Guidance_Intro.md) on how to use AGSi, including guidance on best practice

!!! Note
    This standard concisely sets out requirements for AGSi and provides definitive reference information for the schema. Newcomers to AGSi will likely need to read this in conjunction with the [Guidance](./Guidance_Intro.md) which provides information on how to use AGSi, including examples.

### Navigation of the documentation

Use the navigation menus on all pages to move around the documentation. For the Standard and Guidance, pages of content are generally only found on the third (lowest) level of the menu with the second level used for section headings only. The top level differentiates between the standard, Guidance and Codes/vocabularies (see below), all of which are accessed from this same site.

Internal hyperlinks are used to identify related content on other pages. Use your browser back button to return to the page you were on before you clicked on a link.

### Interpretation of this standard

This standard adopts the common convention used in international standards whereby **shall** and **must** are used to indicate requirements to be strictly followed to conform to this standard, i.e.  mandatory requirements.

**Should** is used to indicate a recommendation, i.e. not mandatory.

The supplementary documentation is not part of the standard.
Links to such documentation provided in the Standard are informative only.

!!! Note
    Throughout the documentation, *Notes* formatted as seen here, are used to provide additional background information or non-mandatory guidance.

### Governance

This standard has been authored by and is maintained by the *Data Management Working Group* of the
<a href="https://ags.org.uk" target="_blank">Association of Geotechnical and Geoenvironmental Specialists (AGS)</a>

!!!Note
    The AGS is a non-profit making trade association established to improve the profile and quality of geotechnical and geoenvironmental engineering. The membership comprises UK organisations and individuals having a common interest in the business of site investigation, geotechnics, geoenvironmental engineering, engineering geology, geochemistry, hydrogeology, and other related disciplines.

Revision control shall be implemented for this Standard. The Standard defines the schema, therefore the revision number of the standard is also the version number of the schema that it relates to.

The system used for revision numbering broadly follows the principles of
<a href="https://semver.org/" target="_blank">Semantic Versioning</a>
which adopts the *Major.Minor.Patch* convention. For AGSi this has been interpreted thus:

* *Major* versions are reserved for major changes in scope or changes that break backwards compatibility
* *Minor* versions used when functionality is added in a backwards compatible manner
* *Patch* versions used for minor updates or correction of errors in documentation or the schema that do not change intent.

The above applies to the Standard only, although versioning of the [JSON Schema](./Standard_Encoding_JSONSchema.md) files(s) will normally be the same as that of the  standard. Versioning of [Codes and vocabularies](./Codes_Intro.md) and the [Guidance](./Guidance_Intro.md) is separate from that of the standard. Refer to the relevant documentation for further details.

Further details of the system adopted for revision numbering are given in the
[Guidance on this subject](./Guidance_General_Revision.md)
