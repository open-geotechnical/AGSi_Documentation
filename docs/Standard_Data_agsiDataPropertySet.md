


# AGSi Standard / Data

## agsiDataPropertySet

### Object description

[agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) objects are containers for sets of property data, with each set typically corresponding to the property data required for a model element. Sets may be referenced from [agsiModelElement](./Standard_Model_agsiModelElement.md) and other parts of the schema.

The parent object of [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) is [agsiData](./Standard_Data_agsiData.md)

[agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) contains the following embedded child objects: 

- [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md)

[agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) has associations (reference links) with the following objects: 

- [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md)
- [agsiModelElement](./Standard_Model_agsiModelElement.md)
- [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md)
- [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md)

[agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) has the following attributes:


- [propertySetID](#propertysetid)
- [description](#description)
- [remarks](#remarks)
- [agsiDataPropertyValue](#agsidatapropertyvalue)


### Attributes

#### propertySetID
Identifier that will be referenced by [agsiModelElement](./Standard_Model_agsiModelElement.md) and other objects as required. All identifiers used by [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) must be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``PropGCC/W``

#### description
Short description identifying the property set.  
*Type:* string  
*Example:* ``Gotham City Clay, west zone``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``

#### agsiDataPropertyValue
Array of embedded documentSetID object(s)  
*Type:* array (agsiDataPropertyValue object(s))  


