# AGSi Standard / Root

## Root schema overview

The top level (ultimate parent) part of the AGSi schema is called the **root** schema.
This is the container for the remainder of the schema.

The root schema is a single object, known as the **root object**.

For JSON encoding this means that the JSON reduces to a single object
at the top level  which is represented by the outermost curly brackets, thus:

    :::JSON

    {  … everything else is in here …  }

It should be noted that the term *root* will not appear in the JSON file.
It is only the term we use to describe the top level object.

All of the other groups of objects (
[agsProject](./Standard_Project_agsProject.md),
[agsiModel](./Standard_Model_agsiModel.md),
[agsiGeometry](./Standard_Geometry_agsiGeometry.md) and
[agsiData](./Standard_Data_agsiData.md))
are contained within the root object,
together with two additional standalone objects:

* [schema](./Standard_Root_schema.md)
* [file](./Standard_Root_file.md)

This is summarised diagramatically below.

![root summary UML diagram](./images/Root_summary_UML.svg)

In JSON encoding, it will look like this.

    :::JSON
    {
      	"schema": { … },
      	"file": { … },
      	"agsProject":{ … },
      	"agsiModel":[ … ],
      	"agsiGeometry":{ … },
        "agsiData":{ … }
    }

The [schema](./Standard_Root_schema.md) and
[file](./Standard_Root_file.md) objects are both **required**, i.e mandatory.  

All other objects are optional as some or all of these may be required,
depending on the information represented.

No more than one instance of each of the
[agsProject](./Standard_Project_agsProject.md),
[agsiGeometry](./Standard_Geometry_agsiGeometry.md) and
[agsiData](./Standard_Data_agsiData.md)) groups of objects are permitted.
However, more than one instance of the
[agsiModel](./Standard_Model_agsiModel.md) group of objects is permitted,
e.g. an array of objects representing different models.

!!! Note
    Even if only one instance of
    [agsiModel](./Standard_Model_agsiModel.md) is required,
    it must still be encoded as an array (with one object).

The full UML diagram for the **root** object is given below.

![root UML diagram](./images/Root_full_UML.svg)
