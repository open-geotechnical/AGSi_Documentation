# AGSi Home page

## Association of Geotechnical & Geoenvironmental Specialists transfer format for ground model and interpreted data (AGSi)

This is the home page for AGSi documentation. For full contents please browse the
navigation menu on the left. Please also read
[Structure of this documentation](#structure-of-this-documentation) below.

### What is AGSi?

AGSi is a schema and transfer format for the exchange of ground model and
interpreted data, for use in the geotechnical, geological, hydrogeological
and geo-environmental domains.

It has been created by and is maintained by the
<a href="https://www.ags.org.uk/" target="_blank">Association of Geotechnical
and Geoenvironmental Specialists (AGS)</a>,
a UK-based industry organisation.

AGSi complements the existing and well established
<a href="https://www.ags.org.uk/data-format/" target="_blank">AGS format</a>
for factual ground investigation data.

!!! Note
    AGSi was launched at a webinar held on 25 November 2020. A recording of the webinar, which explains what AGSi is all about, will be made available
    <a href="https://www.ags.org.uk/" target="_blank">on the AGS website</a>.

### Status

This is a beta issue of AGSi issued November 2020 for industry comment and feedback.

!!! Note
    The Guidance section is work in progress and right now and is 'a little rough around the edges', e.g. some supporting images missing, some links may not work and some of the examples use a slightly out of date development version of the schema. Please bear with us while we tidy this up over the next few weeks.

### Comments and feedback

AGS welcomes comment, feedback and suggestions from industry practitioners and software developers. All contributions welcome.

The current intent is to use the <a href="http://www.agsdataformat.com/datatransferv4/intro.php" target="_blank">AGS format discussion forum</a> to collate initial comments, feedback and suggestions.

!!! Note
    The AGS discussion forum is currently being revamped, due to be re-launched December 2020. We will update our links and re-advertise once the new forum has gone live.


### Structure of this documentation

This documentation is split into three parts:

* The **Standard** section comprises the formal definition of AGSi,
both the schema and its encoding, together with rules for its intended usage.
The intent is that the Standard will change infrequently.
Version control will be applied.

* The **Codes and vocabularies** section incorporates lists of standard codes
and terms that may, or in some cases must, be used within AGSi.
It is likely that this section will be updated more frequently than the Standard. Such changes will generally only be additive, i.e. backwards compatible, with the changes clearly flagged on the relevant page.

* The **Guidance** provides further information about how AGSi can be used in practice,
including examples.  It is anticipated that we will update and improve the
guidance from time to time, in response to user feedback. Any revisions will be indicated on the relevant page (at the bottom).

!!! Note
    The version indicated on the webpage (top left) is the version of the Standard, which is also the version of the schema. The Codes and vocabularies and Guidance sections are separately version controlled, as indicated on the relevant pages, but the versions provided are intended to be compatible with the version of the Standard indicated on the webpage.
