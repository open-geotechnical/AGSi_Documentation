


# AGSi Standard / Project

## agsProject

### Object description

Metadata for the specific project/commission (the [Project](./Standard_General_Definitions.md#project)) under which this data set has been delivered. The parent project, including the ultimate parent project, may be identified using the relevant attributes. 

The parent object of [agsProject](./Standard_Project_agsProject.md) is [root](./Standard_Root_Intro.md)

[agsProject](./Standard_Project_agsProject.md) contains the following embedded child objects: 

- [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md)
- [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md)
- [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md)
- [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md)

[agsProject](./Standard_Project_agsProject.md) has the following attributes:


- [projectName](#projectname)
- [producer](#producer)
- [producerSuppliers](#producersuppliers)
- [client](#client)
- [description](#description)
- [projectCountry](#projectcountry)
- [producerProjectID](#producerprojectid)
- [clientProjectID](#clientprojectid)
- [parentProjectName](#parentprojectname)
- [ultimateProjectName](#ultimateprojectname)
- [ultimateProjectClient](#ultimateprojectclient)
- [briefDocumentSetID](#briefdocumentsetid)
- [reportDocumentSetID](#reportdocumentsetid)
- [agsProjectCoordinateSystem](#agsprojectcoordinatesystem)
- [agsProjectInvestigation](#agsprojectinvestigation)
- [agsProjectDocumentSet](#agsprojectdocumentset)
- [agsProjectCodeSet](#agsprojectcodeset)
- [remarks](#remarks)


### Attributes

#### projectName
Name of the specific project/commission for the [Project](./Standard_General_Definitions.md#project)  
*Type:* string  
*Condition:* Required  
*Example:* ``C999 Geotechnical Package X``

#### producer
Organisation employed by the client responsible for the [Project](./Standard_General_Definitions.md#project)  
*Type:* string  
*Condition:* Recommended  
*Example:* ``ABC Consultants``

#### producerSuppliers
If applicable, subconsultant(s) or subcontractor(s) employed  on the [Project](./Standard_General_Definitions.md#project). Typically only include suppliers with direct involvement in producing the data included in this file. Input required as a text string not an array.  
*Type:* string  
*Example:* ``Acme Environmental, AN Other Organisation``

#### client
Client for the [Project](./Standard_General_Definitions.md#project)  
*Type:* string  
*Condition:* Recommended  
*Example:* ``XYZ D&B Contractor``

#### description
Brief project description.  
*Type:* string  
*Example:* ``Stage 3 sitewide ground modelling, including incorporation of new 2018 GI data.``

#### projectCountry
Normally the country in which the ultimate project is taking place.  
*Type:* string  
*Example:* ``United Kingdom``

#### producerProjectID
Identifier for this Project used by the producer of this file   
*Type:* string  
*Example:* ``P12345``

#### clientProjectID
Identifier for this Project used by the client  
*Type:* string  
*Example:* ``C999/ABC``

#### parentProjectName
If applicable, the parent project/commission under which the [Project](./Standard_General_Definitions.md#project) has been procured, or which the [Project](./Standard_General_Definitions.md#project) reports to  
*Type:* string  
*Example:* ``C999 Area A Phase 1 Design and Build``

#### ultimateProjectName
If applicable, the top level parent project that the [Project](./Standard_General_Definitions.md#project) is ultimately for. Typically the works that are to be constructed, or a framework.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Gotham City Metro Purple Line``

#### ultimateProjectClient
Client for the top level parent project  
*Type:* string  
*Example:* ``City Transport Authority``

#### briefDocumentSetID
Reference to the brief and/or specification for the project, details of which should be provided by way of an [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object  
*Type:* string (reference to documentSetID of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object)  
*Example:* ``SpecProject``

#### reportDocumentSetID
Reference to report(s) and other documentation produced as part of this project and/or provided in support of this data set.   
*Type:* string (reference to documentSetID of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object)  
*Example:* ``ReportProject``

#### agsProjectCoordinateSystem
Array of embedded [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) object(s)  
*Type:* array (agsProjectCoordinateSystem object(s))  


#### agsProjectInvestigation
Array of embedded [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) object(s)  
*Type:* array (agsProjectInvestigation object(s))  


#### agsProjectDocumentSet
Array of embedded [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object(s)  
*Type:* array (agsProjectDocumentSet object(s))  


#### agsProjectCodeSet
Array of embedded [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object(s)  
*Type:* array (agsProjectCodeSet object(s))  


#### remarks
Additional remarks if required  
*Type:* string  
*Example:* ``Some remarks if required``

