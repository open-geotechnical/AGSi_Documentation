# AGSi Standard / General

## General rules and conventions

This section comprises general rules and conventions that apply across the entire schema.

!!! Note
    Use the menu top right of the page to quickly navigate to the item of interest.

### Character encoding

AGSi data shall use **UTF-8** encoding in accordance with the current version of the
<a href="https://unicode.org/standard/standard.html" target="_blank">Unicode Standard</a> (ISO/IEC 10646).

### Case sensitivity

AGSi **object and attribute names are case sensitive** and must be reproduced exactly as specified in this standard.

All **text string values** in an AGSi file shall, bar the exceptions below, be considered as **not case sensitive**, i.e. the meaning is the same regardless of the case. However, applications should aim to preserve the original case as this often assists readability.

The **exception**, i.e. text that should be considered to be case sensitive, is:

* **[units](./Standard_General_Formats.md#units) of measure**, e.g. mg and Mg have different meanings

!!! Note
    Unfortunately, validation using
    <a href="https://json-schema.org/" target="_blank">JSON Schema</a> assumes case sensitivity. This could be a problem where the value required is a standard term from a restricted list of values (using JSON Schema *enum* keyword). A valid value input in a different case may be incorrectly rejected during validation.  


### Data types

Attribute data values shall be one of the following data types:

* string
* number
* boolean
* null
* array
* object

!!! Note
    The above replicate
    [allowable JSON data types](./Guidance_Encoding_Json.md#json-data-types).

Refer to the
[Encoding section](./Standard_Encoding_JSON.md#data-types-and-format)
for specific requirements and limitations applicable to each data type when encoded.

For AGSi, an array is a list, i.e. a one dimensional array. Further requirements relating to
[array](#arrays) and
[object](#embedded-objects-or-arrays-of-objects) data types are given below.

For some attributes, additional requirements or formats for data are specified. In the documentation (object reference pages) such requirements are given in brackets after the data type, and may include:

* Identifier
* Reference to (identifier for) another object
* Standard term from list
* Recommended term
* Date and/or time
* URI (for file/web links)
* Coordinate tuple
* Profiles / array of coordinates

Guidance on the requirements for such formats is provided in the
[Data input formats](./Standard_General_Formats.md) section.

### Arrays

Some attributes are of **array** type. An array is a list.

Each member of the array shall be assumed to be of *string* data type unless otherwise specified thus:

*Type:* array (data type for array member)

Where the schema specifies an array type for an attribute, the data must be encoded as an array, even if the data requires only one value (or object).
Replacing the array with a single value/object is not permitted.

The way that an array is expressed in AGSi output will depend on the method of encoding. For the purposes of the examples shown in this documentation, JSON encoding has been assumed. In this case, an array is expressed using square brackets and commas as follows:

    :::JSON
    ["List item 1" ,  "List item 2" , "List item 3"]

The above example shows a list of strings which require enclosing quotemarks. Lists of numbers do not require quotemarks. In addition, the above includes white space added for readability. This would be ignored in processing.

### Lists as text strings

In some cases the schema asks for data
that represents a list to be input as a single text
string, not an array.
In such cases, use of an array is not permitted.

!!! Note
    The schema generally requires arrays where identification
    of the separate values may be critical to correct reading, parsing or understanding of the data.

    Lists as simple text string lists are used for attributes that carry
    metadata that does not normally need to be parsed to a list of items,
    e.g. [producerSuppliers](./Standard_Project_agsProject.md#producersuppliers) in
    [agsProject](./Standard_Project_agsProject.md).

    The schema documentation makes it clear whether an array is required or not.


### Embedded objects or arrays of objects

Where an object data type is specified, the attribute value (parent) shall comprise an instance of the specified object (child), e.g.

    :::JSON
    { parentAttribute:
      { childAttribute: "a value", anotherChildAttribute: "a value"}
    }

Where an array of objects is specified, the array shall comprise the specified object instances, e.g.

    :::JSON
    { parentAttribute:
      [ {childAttribute: "a value", anotherChildAttribute: "a value"},
      {childAttribute: "another value", anotherChildAttribute: "another value"} ]
    }

### Required attributes

Where the AGSi schema specifies that an attribute is **required**, then this attribute must be included in the encoded file.
Generally this attribute will be required to have a value defined or contain an embedded object, as applicable. However, a null value or blank string value may be acceptable where specified in the schema documentation (object reference).

### Empty or null values

For attributes that are not specified as required,
where the value of an attribute is **null or blank**, or there is no object to embed, then
the attribute name should be omitted from the data for the relevant object instance.

Use of the *null* data type for null or blank data is not permitted unless otherwise stated in the schema documentation (object reference).

For string data, use of a blank string value, i.e: ``""``, is permitted as an alternative to omission, e.g.

    :::JSON
    { "blankAttributeName" : "" }


### Characters requiring special treatment in strings

To comply with the requiremesnt of JSON encoding
(see [Encoding / JSON](./Standard_Encoding_JSON.md))
certain characters cannot be used in string (text) data unless escaped by prefixing with a **\\** (backslash). This includes the following:

* Newline to be replaced with **\n**
* Carriage return to be replaced with **\r**
* Tab to be replaced with **\t**
* Double quote to be replaced with **\\"**
* Backslash to be replaced with **\\\\**

Note that **a single quote/apostrophe (') should not be escaped**.

!!! Note
    Attention is drawn to line breaks (ASCII chr(10)) which should be encoded as **\\n**.   

### Identifiers

Identifiers are used to facilitate cross referencing between different objects.
Objects that may be referenced by other objects have an attribute assigned as an **identifier** that can be referenced by other objects. Such attributes are clearly identified in the schema documentation. They are usually the first in the list and have a name ending in *...ID*.

The value assigned to the identifier can be any string value. However, to ensure that cross-reference links are unambiguous, all object instances that may be referenced by a particular attribute must have unique identifiers.

For example, an [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) object has an identifier attribute *[propertySetID](./Standard_Data_agsiDataPropertySet.md#propertysetid)* which may be referenced by the
*[propertySetID](./Standard_Model_agsiModelElement.md#propertysetid)* attribute of an
[agsiModelElement](./Standard_Model_agsiModelElement.md) object.
The *[propertySetID](./Standard_Data_agsiDataPropertySet.md#propertysetid)* identifiers used for each instance of the [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) object must be unique.

Particular care must be taken with identifiers used in the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group as the
*[geometryID](./Standard_Model_agsiModelElement.md#geometryid)* attribute of [agsiModelElement](./Standard_Model_agsiModelElement.md) may reference any of the object types in [agsiGeometry](./Standard_Geometry_agsiGeometry.md).  Therefore all *geometryID* identifiers for all objects in the [agsiGeometry](./Standard_Geometry_agsiGeometry.md)
group must be unique.

It is not necessary for all identifiers in a file to be unique. However, adoption of a system where all identifiers are unique will reduce the risk of error.

For large or complex data sets, or where there is a likelihood that data sets may be combined at a later date, e.g. on a large project, then the use of UUIDs for identifiers, further described below, is recommended.

Conventions and systems for assigning identifiers should be agreed by users at an early stage, preferably as part of the specification.

### UUIDs

A universally unique identifier (UUID) is a 128-bit number used to identify information in computer systems. The term globally unique identifier (GUID) is also often used.

When generated according to the standard methods, UUIDs are, for practical purposes, unique. The standard describing UUIDs is
<a href="https://tools.ietf.org/html/rfc4122" target="_blank">RFC 4122</a>.

A typical UUID looks like this: <br>
``f90c130b-6f93-4a8b-a4e4-d774ecd776d8``

AGSi does not require the use of UUIDs. However, for some attributes use of UUIDs is recommended as good practice. Details are provided in the schema documentation (object reference).


### Coordinate systems

Each model shall have a coordinate system defined. Models may share the same coordinate system, but a single model may only have one coordinate system defined.

The coordinate system(s) used by the model is considered to be a ***model coordinate system***, although this could be an established regional or national system.

A secondary ***global coordinate system***, which will normally be an established regional or national system, may also be defined but this will only exist via transformation from the model coordinate system.

Coordinate systems, include the units of measurement used, are defined using
[agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) objects.  See also the [Project rules and conventions](./Standard_Project_Rules.md) and [Model rules and conventions](./Standard_Model_Rules.md) for further details and requirements.

Where applicable, the units of measurement for spatial distances, e.g. length, height or depth, are those implied by coordinate system.
