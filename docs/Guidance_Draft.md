# AGSi Guidance

## WIP page

This is an unpublished collection of deleted snippets - for possible future re-uses

### Family of AGS schemas

The intention is for AGSi to become of one a family of similar AGS schemas covering ground investigation, monitoring and ground related construction.

The current AGS format for factual GI data is not compatible with AGSi, but the aspiration is for it to be adapted to this style of schema at its next major revision. None of the other proposed schemas exist at present although some, such as a schema for piling data, are in development.

The current proposal is for all members of this family to share 'common' objects where appropriate. Examples include general metadata such as project/investigation data and file/transmittal information. The AGSi schema presented here includes
[agsProject](./agsProject.md) which is expected to be one of the future common object groups.
The [file](./Standard_Root_file.md) and
[schema](./Standard_Root_schema.md) objects of the
[root schema](/Standard_Root_Intro) are also
currently expected to become common objects.

This concept is illustrated below:

![general](./images/general_family.svg)
