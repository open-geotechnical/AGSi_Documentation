# AGSi Standard / Geometry

## Usage and schema overview

### Usage

The 'Geometry' part of the schema is the repository for geometry data
associated with model objects, most commonly [agsiModelElement](./Standard_Model_agsiModelElement.md)
objects but also some [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) objects.

The geometry required by the [agsiModelElement](./Standard_Model_agsiModelElement.md) or [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) objects
is identified by reference to the *[geometryID](./Standard_Model_agsiModelElement.md#geometryid)* attribute for the
relevant target geometry object.

The reason for separating geometry from the model and linking it by reference
is that it makes it possible to re-use the same geometrical data for
a number of different model elements, thus avoiding duplication of
the same data. It also means that the model part of the schema can
be lightweight and easy to follow.

There are several different types of geometry objects.
These are summarised on the
[Geometry object summary page](./Standard_Geometry_Summary.md).

### Summary of schema

The objects in the Geometry group mostly represent different types of geometry, as described in the [Geometry object summary](./Standard_Geometry_Summary.md).
The exceptions are:

- [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) is a pointer to geometry information provided in an external file (includes URI link and file metadata)
- [agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) defines collections (sets) of exploratory holes ([agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md))
- [agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md) similarly defines collections (sets) of stratigraphical column segments ([agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md))

The [agsiGeometry](./Standard_Geometry_agsiGeometry.md) object is the container for all Geometry group objects.
All of the objects are directly embedded in [agsiGeometry](./Standard_Geometry_agsiGeometry.md), except for
[agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) and
[agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md) which are embedded within there respective 'set' objects,
[agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md)
and
[agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md).

There are additional reference links between some of the objects.

This diagram shows the objects in the Geometry group and their relationships.

![Geometry summary UML diagram](./images/agsiGeometry_summary_UML.svg)


### Schema UML diagram

This diagram shows the full schema of the Geometry group including all attributes.

!!! Note
    Reference links to other parts of the AGSi schema are not shown in this diagram.

![Geometry full UML diagram](./images/agsiGeometry_full_UML.svg)
