


# AGSi Standard / Model

## agsiModelBoundary

### Object description

An [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) object defines the model boundary, i.e. the maximum extent of the model. Any elements or parts of elements lying outside the boundary are deemed to not be part of the model. Only one boundary per [agsiModel](./Standard_Model_agsiModel.md) is permitted. Only plan boundaries with vertical sides are permitted, defined by either limiting coordinates, or a bounding close polygon. The top and base may be either a flat plane at a defined elevation, or a surface. Top boundary may not be required, depending on nature of model and/or software/application used (to be confirmed in specification). 

The parent object of [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) is [agsiModel](./Standard_Model_agsiModel.md)

[agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) has associations (reference links) with the following objects: 

- [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md). [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md)
- agsiPlane

[agsiModelBoundary](./Standard_Model_agsiModelBoundary.md) has the following attributes:


- [description](#description)
- [minX](#minx)
- [maxX](#maxx)
- [minY](#miny)
- [maxY](#maxy)
- [topElevation](#topelevation)
- [bottomElevation](#bottomelevation)
- [boundaryXYGeometryID](#boundaryxygeometryid)
- [topSurfaceGeometryID](#topsurfacegeometryid)
- [bottomSurfaceGeometryID](#bottomsurfacegeometryid)
- [remarks](#remarks)


### Attributes

#### description
Short description.  
*Type:* string  
*Example:* ``Boundary for Geological Model: sitewide``

#### minX
Minimum X for box boundary  
*Type:* number  
*Example:* ``20000``

#### maxX
Maximum X for box boundary  
*Type:* number  
*Example:* ``35000``

#### minY
Minimum Y for box boundary  
*Type:* number  
*Example:* ``10000``

#### maxY
Maximum Y for box boundary  
*Type:* number  
*Example:* ``15000``

#### topElevation
Elevation (Z) of top plane of model for box boundary  
*Type:* number  
*Example:* ``40``

#### bottomElevation
Elevation (Z) of bottom plane of model for box boundary  
*Type:* number  
*Example:* ``-40``

#### boundaryXYGeometryID
Reference to geometry of (closed) polygon defining plan extent of model, as an alternative to the box boundary. Use with caution as this may not be supported by all software/applications. Confirm use in specification.  
*Type:* string (reference to geometryID of [agsiGeometryLine](./Standard_Geometry_agsiGeometryLine.md) or [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object)  
*Example:* ``GeometryModelBoundary``

#### topSurfaceGeometryID
Reference to geometry of surface defining top of model, as an alternative to the box boundary. Use with caution as this may not be supported by all software/applications. May not be required for some software/applications. Confirm use in specification.  
*Type:* string (reference to geometryID of  [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) or [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) object)  
*Example:* ``GeometryTerrain``

#### bottomSurfaceGeometryID
Reference to geometry of surface defining base of model, as an alternative to the box boundary. Use with caution as this may not be supported by all software/applications. Confirm use in specification.  
*Type:* string (reference to geometryID of  [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) or [agsiGeometryPlane](./Standard_Geometry_agsiGeometryPlane.md) object)  
*Example:* ``GeometryModelBase``

#### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

