# AGSi Standard / Project

## Usage and schema overview

### Usage

The Project part of the schema contains general projectwide data.
This includes:

* Project metadata
* Investigation metadata
* Coordinate systems used (by the models)
* Documents, that may be referenced by other parts of the schema
* Codes, used by other parts of the schema

!!! Note
    The namespace prefix *ags* is used for the Project group,
    as opposed to *agsi*, i.e. *agsProject*, not *agsiProject*.
    This is because the intent is for the agsProject group to
    eventually be shared with other AGS schema
    that may be developed in the future (AGS family of schema).

#### Projects and investigations

For the purposes of AGSi, the **Project** is the
specific project/commission under which this data set has been delivered.

The *Project* is most likely to be just one component of an **ultimate project**, typically the works that are eventually to be constructed, that will benefit from the *Project*. Alternatively the *ultimate project*
may be a framework contract set up by an asset owner.

In some cases the *Project* will be directly procured by the *ultimate project*. However, in some cases the
*Project* may be procured under an intermediate
**parent project**. Sometimes there may be further tiers of parent project.

Regardless of the relationship between the *Project*,  *parent project(s)* and *ultimate project*,
the *Project* for the purposes of AGSi will be
as defined above. Information on the *ultimate project*
and, if applicable, *parent project(s)* should be provided
as part of the metadata for the *Project*.

An **investigation** is a campaign of fieldwork and/or
laboratory testing carried out and reported under a site/ground investigation contract.
An investigation will itself be a project, and in some cases
this may be the *Project* under which the AGSi has been delivered.
However, it will commonly be the case that the AGSi ground models and/or interpreted data are based on data from multiple
*investigations*, which may or may not be contractually
linked to the *Project*. Therefore the schema separates the
*Project* (for AGSi) from the *investigation(s)* that the
AGSi data is based on. The AGSi data set should include
information on both.

Refer to [Project rules and conventions](./Standard_Project_Rules.md)
for further details.

#### Coordinate systems

This part of the schema is used to define the
spatial coordinate system(s) used by the models.

The coordinate system used by a model is considered to be the ***model coordinate system***, although this could be an established regional or national system.

A secondary ***global coordinate system***, which will normally be an established regional or national system, may also be defined.
This will exist via transformation from the *model coordinate system*.

Refer to [Project rules and conventions](./Standard_Project_Rules.md)
for further details.

#### Project documents

This part of the schema is used to identify relevant documents including a link to each document. This may be  a relative link to an included file or a link to an external file, typically linking to a project document management system that users have will access to.

The documents are organised into sets. These sets can be referenced from other parts of the schema.

AGS (factual) data files are considered to be documents and should be included here.

Refer to [Project rules and conventions](./Standard_Project_Rules.md)
for further details.   

#### Code sets

Some parts of the schema, e.g.
[agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md),
require the use of project specific codes. These are typically used for standard or project specific ABBR codes inherited from AGS factual data.

These codes are defined here, in the project group. The codes are grouped into sets corresponding to the object/attribute that uses them. The codes used in a set may be individually identified, or reference made to an external list of codes.

Refer to [Project rules and conventions](./Standard_Project_Rules.md)
for further details.

### Summary of schema

This diagram shows the objects in the Project group and their relationships.

![Project summary UML diagram](./images/agsProject_summary_UML.svg)

[agsProject](./Standard_Project_agsProject.md) includes
general metadata for the *Project* and is a container
for all of the other objects described below.

Coordinate systems used by models are defined using [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md) objects.
If all models are based on the same coordinate system
then only one system (one object) is required.

The *investigations* that the data is based on are defined using  
[agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md)
objects.

*Sets of* key project *documents* may be identified using
[agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md)
objects. These document sets can be refenced from several different parts of the AGSi schema by referencing the
[documentSetID](./Standard_Project_agsProjectDocumentSet.md#documentsetid)
identifier assigned.

The individual *documents* making up each set are defined using  [agsProjectDocument](./Standard_Project_agsProjectDocument.md)
objects. These document objects are collected together (embedded within) the relevant parent [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object.

*Sets of codes* used by other parts of the schema may be identified using
[agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) objects.
The schema object and attribute that uses each code list is identified within this object.

If required, the individual codes making up each set are defined using  [agsProjectCode](./Standard_Project_agsProjectCode.md) objects.
These codes are collected together (embedded within) the relevant parent [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) object.

Refer to [Project rules and conventions](./Standard_Project_Rules.md)
for further details of how the above should be used in practice.

### Schema UML diagram

This diagram shows the full schema of the Project group including all attributes.

!!! Note
    Reference links to other parts of the AGSi schema are not shown in this diagram.

![Project full UML diagram](images/agsProject_full_UML.svg)
