
# AGSi Standard / Data

## Usage and schema overview

### Usage

The 'Data' part of the schema is intended to be the repository for
property and parameter data associated with model elements.

The data contained here will be linked from other parts of
the schema, in particular [agsiModelElement](./Standard_Model_agsiModelElement.md), by reference.

!!! Note
    This structure has been adopted because real life data can be extensive and may require complex data structures to express it correctly and efficiently. In addition, this structure allows the same data to be referenced from different parts of the schema.

For the purposes of the schema, a distinction between properties and
parameters has been made as follows:

#### Property

A value reported from an observation or test (directly measured, or based on interpreted measurement), as typically reported in a factual GI report. Properties are likely to be (Eurocode 7) measured or derived values.

#### Parameter

An interpreted single value, or a profile of values related to an independent variable, that is to be used in design and/or analysis. Parameters are most likely to be (Eurocode 7) characteristic values.

!!! Note
    AGSi supports the reporting of summaries of the property data including basic statistical data. There is also limited support for reporting of individual test results where this is required for the model.

    However, it is not the intent of AGSi to include full factual data as this is covered by the AGS (factual) format


### Summary of schema

This diagram shows the objects in the Data group and their relationships.

![Data summary UML diagram](./images/agsiData_summary_UML.svg)

[agsiData](./Standard_Data_agsiData.md) is a container for all of the other objects described below.

The values of individual parameters are included as [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) objects. These parameter values are then collected into sets of parameters in [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) objects. To achieve this, agsDataParameterValue objects are embedded within [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md), i.e. this is a heirarchical parent-child relationship. Each [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) object will likely have many [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) objects embedded within it.

agiDataPropertyValue and [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) operate in identical manner to [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) and [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) but are used for properties instead of parameters.

Other parts of the AGSi schema link to data via [agsiDataParameterSet](./Standard_Data_agsiDataParameterSet.md) and [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md), i.e. model elements, features or report elements link to sets of data that are applicable to that element or feature. Refer to the guidance on usage for examples.

The properties and parameters are referenced using codes. The codes are
defined using [agsiDataCode](./Standard_Data_agsiDataCode.md) objects linked from agsDataParameterValue or agiDataPropertyValue.

Properties and parameters can also be assigned to a 'case' using the relevant attributes in agsDataParameterValue or agiDataPropertyValue Codes may be assigned for cases which can be defined using [agsiDataCase](./Standard_Data_agsiDataCase.md) objects.

For more guidance on how to use this part of the schema, see
[Data - Guidance on usage](./Guidance_Data_General.md)


### Schema UML diagram

This diagram shows the full schema of the Data group including all attributes.

!!! Note
    Reference links to other parts of the AGSi schema are not shown in this diagram.

![Data full UML diagram](images/agsiData_full_UML.svg)
