


# AGSi Standard / Geometry

## agsiGeometryLayer

### Object description

An [agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md) object is a volumetric element bounded by two infinite horizontal planes at specified elevations. May be used for defining each element in a stratigraphical column (one dimensional) model. See [Geometry rules and conventions](./Standard_Geometry_Rules.md) for interpretation.

The parent object of [agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md) is [agsiGeometry](./Standard_Geometry_agsiGeometry.md)

[agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md) has associations (reference links) with the following objects: 

- [agsiModelElement](./Standard_Model_agsiModelElement.md)

[agsiGeometryLayer](./Standard_Geometry_agsiGeometryLayer.md) has the following attributes:


- [geometryID](#geometryid)
- [description](#description)
- [topElevation](#topelevation)
- [bottomElevation](#bottomelevation)
- [remarks](#remarks)


### Attributes

#### geometryID
Identifier that will be referenced by [agsiModelElement](./Standard_Model_agsiModelElement.md). All identifiers used within the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``DesignPileGCC``

#### description
Short description of geometry defined here  
*Type:* string  
*Example:* ``Design profile, pile design: Gotham City Clay``

#### topElevation
Elevation (z) of the top surface. Definition of both top and bottom surfaces is recommended to minimise the risk of error.  
*Type:* number  
*Condition:* Required if bottomElevation not used  
*Example:* ``6``

#### bottomElevation
Elevation (z) of the bottom surface. Definition of both top and bottom surfaces is recommended to minimise the risk of error.  
*Type:* number  
*Condition:* Required if topElevation not used  
*Example:* ``-30``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``

