


# AGSi Standard / Data

## agsiDataCode

### Object description

[agsiDataCode](./Standard_Data_agsiDataCode.md) is a dictionary for the property and parameter codes referenced by other objects. It also includes the units applicable for each property/parameter. Use of codes from the [AGSi code list](./Codes_Codelist.md) is recommended. Replication of the codes from a published dictionary such as the [AGSi code list](./Codes_Codelist.md) is optional provided that the dictionary used is identified in [agsiData](./Standard_Data_agsiData.md) (codeDictionary attribute). If published codes are replicated here, only those used in the file/data set should be included. Any user defined (non standard) codes must be included, with the isStandard flag set as false.

The parent object of [agsiDataCode](./Standard_Data_agsiDataCode.md) is [agsiData](./Standard_Data_agsiData.md)

[agsiDataCode](./Standard_Data_agsiDataCode.md) has associations (reference links) with the following objects: 

- [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md)
- [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md)

[agsiDataCode](./Standard_Data_agsiDataCode.md) has the following attributes:


- [codeID](#codeid)
- [description](#description)
- [units](#units)
- [isStandard](#isstandard)
- [remarks](#remarks)


### Attributes

#### codeID
Identifying code for a property or parameter. Use of codes from a standard dictionary, such as the [AGSi code list](./Codes_Codelist.md), is recommended. Referenced by [agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) and/or [agsiData](./Standard_Data_agsiData.md) PropertyValue   
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``UndrainedShearStrength``

#### description
Name or short description of what the code represents.  
*Type:* string  
*Condition:* Required  
*Example:* ``Undrained shear strength``

#### units
Units of measurement for this property or parameter. An empty string entry is required if there are no units (dimensionless).  
*Type:* string (units)  
*Condition:* Required (but may be empty)  
*Example:* ``kPa``

#### isStandard
true if code is from standard dictionary such as the [AGSi code list](./Codes_Codelist.md), i.e. user defined code for this project. If omitted, should be assumed to be false, i.e. project specific or other non-standard code  
*Type:* boolean  
*Condition:* Recommended  
*Example:* ``true``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``

