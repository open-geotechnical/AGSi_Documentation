


# AGSi Standard / Model

## agsiModelSubset

### Object description

Used to identify optional subsets of the model. Subsets are created by reference to these objects from [agsiModelElement](./Standard_Model_agsiModelElement.md). To be used with caution as subsets may not be supported by all software/applications. Use of subsets should be clarified in the specification. 

The parent object of [agsiModelSubset](./Standard_Model_agsiModelSubset.md) is [agsiModel](./Standard_Model_agsiModel.md)

[agsiModelSubset](./Standard_Model_agsiModelSubset.md) has associations (reference links) with the following objects: 

- [agsiModelElement](./Standard_Model_agsiModelElement.md)

[agsiModelSubset](./Standard_Model_agsiModelSubset.md) has the following attributes:


- [subsetID](#subsetid)
- [description](#description)
- [remarks](#remarks)


### Attributes

#### subsetID
Identifier referenced by [agsiModelElement](./Standard_Model_agsiModelElement.md).  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``GeologyUnitVolumes``

#### description
Short description.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Geological unit volumes``

#### remarks
Additional remarks, if required.  
*Type:* string  
*Example:* ``Some additional remarks``

