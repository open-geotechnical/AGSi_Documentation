


# AGSi Standard / Project

## agsProjectInvestigation

### Object description

Basic metadata for investigations, generally ground investigations, relevant to the file/data set. Referenced from various parts of the schema. More detailed metadata may be provided using an [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) object referenced from here.

The parent object of [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) is [agsProject](./Standard_Project_agsProject.md)

[agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) has associations (reference links) with the following objects: 

- [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md)

[agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) has the following attributes:


- [investigationID](#investigationid)
- [investigationName](#investigationname)
- [description](#description)
- [contractor](#contractor)
- [client](#client)
- [engineer](#engineer)
- [parentProjectName](#parentprojectname)
- [ultimateProjectName](#ultimateprojectname)
- [ultimateProjectClient](#ultimateprojectclient)
- [subcontractors](#subcontractors)
- [fieldworkDateStart](#fieldworkdatestart)
- [scopeDescription](#scopedescription)
- [locationCoordinateProject](#locationcoordinateproject)
- [locationCoordinateGlobal](#locationcoordinateglobal)
- [locationDescription](#locationdescription)
- [specificationDocumentSetID](#specificationdocumentsetid)
- [reportDocumentSetID](#reportdocumentsetid)
- [dataDocumentSetID](#datadocumentsetid)
- [propertySetID](#propertysetid)
- [remarks](#remarks)


### Attributes

#### investigationID
Internal identifier for this investigation. Referenced by other parts of the schema such as [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md).  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``GIPackageA``

#### investigationName
Name of investigation  
*Type:* string  
*Condition:* Required  
*Example:* ``Gotham City Metro Purple Line, C999 Package A``

#### description
Further description of investigation, if required  
*Type:* string  
*Example:* ``Preliminary sitewide investigation, March-July 2018``

#### contractor
Contractor undertaking the investigation  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Boring Drilling Ltd``

#### client
Commissioning (contracting) client for the investigation  
*Type:* string  
*Condition:* Recommended  
*Example:* ``XYZ D&B Contractor``

#### engineer
Organisation acting as Engineer, Investigation Supervisor, Contract Administrator or equivalent. If technical and contractual roles are split, then include both.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``ABC Consultants``

#### parentProjectName
Name of the parent project that this investigation is for. If parent is ultimate parent project, then may be left blank.  
*Type:* string  
*Example:* ``C999 Area A Phase 1 Design and Build``

#### ultimateProjectName
Name of the ultimate parent project that this investigation is for  
*Type:* string  
*Condition:* Recommended  
*Example:* ``Gotham City Metro Purple Line``

#### ultimateProjectClient
Client for the ultimate parent project  
*Type:* string  
*Example:* ``City Transport Authority``

#### subcontractors
List of significant subcontractors or suppliers working on the investigation. List as a text string, not an array.  
*Type:* string  
*Example:* ``Acme Specialist Lab, XYZ Environmental Lab``

#### fieldworkDateStart
Date of start of fieldwork. Date in ISO 8601 format so it could be to nearest month (2019-05) or just the year if exact date not available.  
*Type:* string (date)  
*Condition:* Recommended  
*Example:* ``2018-08-21``

#### scopeDescription
Brief description of scope  
*Type:* string  
*Example:* ``Preliminary investigation comprising 27 boreholes of which 15 extended by rotary coring up to 55m depth, 45 CPT (max 15m), 35 trial pits, 26 dynamic sampling holes, geotechnical and contamination sampling and testing, piezometric monitoring, limited gas monitoring.``

#### locationCoordinateProject
Coordinates, in a relevant [model coordinate system](./Standard_General_Definitions.md#model-coordinate-system) (if applicable), of a point that represents the general location of the investigation, typically the middle of the site. Relevant system will be usually be a 3D system that is used for all 3D models, or a 2D map system. Do not use this attribute if multiple different 3D model systems are in use.  
*Type:* array (coordinate tuple)  
*Example:* ``[25500,13200]``

#### locationCoordinateGlobal
Coordinates, in [global coordinate system](./Standard_General_Definitions.md#global-coordinate-system) (national or regional system, as defined in [agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md)) of a point that represents the general location of the investigation, typically the middle of the site.  
*Type:* array (coordinate tuple)  
*Example:* ``[475270,137965]``

#### locationDescription
Brief description that locates the site. Could be a postal address.  
*Type:* string  
*Example:* ``Gotham City, west, central and southeast``

#### specificationDocumentSetID
Reference to the specification for the GI, details of which should be provided in an [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object  
*Type:* string (reference to documentSetID of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object)  
*Example:* ``GIPackageASpec``

#### reportDocumentSetID
Reference to the report(s), details of which should be provided in an [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object  
*Type:* string (reference to documentSetID of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object)  
*Example:* ``GIPackageAReport``

#### dataDocumentSetID
Reference to the data for the GI, typically the AGS (factual) data, details of which should be provided in an [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object.  
*Type:* string (reference to documentSetID of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) object)  
*Example:* ``GIPackageAData``

#### propertySetID
Reference to an [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) object. Used to provide further metadata relating to the investigation, if required.  
*Type:* string (reference to propertySetID of [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) object)  
*Example:* ``PropGIGeneralA``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some additional remarks``

