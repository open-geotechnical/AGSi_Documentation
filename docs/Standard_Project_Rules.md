# AGSi Standard / Project

## Project rules and conventions

This section details rules and conventions applicable to the Project group of objects.
It shall be read in conjunction with the relevant object reference sections.

This section shall also be read in conjunction with the [General rules and conventions](./Standard_General_Rules.md) that apply to all parts of the AGSi schema.


### Projects and investigations

For the purposes of AGSi, the **Project** is the
specific project/commission under which this data set has been delivered.

Information on the **ultimate project**
and, if applicable, **parent project(s)** should be provided
as part of the metadata for the *Project*.

An **investigation** is a campaign of fieldwork and/or
laboratory testing carried out and reported under a site/ground investigation contract.

If an investigation is also the *Project*, it is recommended that full details be provided in both the
[agsProject](./Standard_Project_agsProject.md) and the  [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md) objects.

### Coordinate systems

#### Types of coordinate system supported

AGSi supports only cartesian systems. The following variants may be used:

- XYZ (3D)
- XZ (2D vertical section)
- XY (2D map)
- Z (elevation only, e.g. simple 1D layer profiles).

Decimal units of measure must be used. Use of latitude and longitude (degrees/minutes/second) is not directly  supported by AGSi.

For XYZ and XY systems, the units of measure used for the X and Y axis must be the same.

#### Model coordinate system(s)

The coordinate system used by a model is considered to be the **model coordinate system**, although this could be an established regional or national system.

If some or all models in an AGSi file/data share the same system, then this system only needs to be defined once in the file/data set. If this is the case, the single
[agsProjectCoordinateSystem](./Standard_Project_agsProjectCoordinateSystem.md)
object must be included within an array given that the parent attribute data type is array, in accordance with
[General rules and conventions](./Standard_General_Rules.md#arrays).
Replacing the array with a single object is not permitted.

The coordinate system(s) to be used for the project should be described in the
[specification](./Standard_General_Specification.md).

#### Global coordinate system transformation

A secondary **global coordinate system**, which will normally be an established regional or national system, may also be defined. This will only exist via transformation from the *model coordinate system*.

Usage of the attributes for transformation is defined in the [agsProjectCoordinateSystem object reference](./Standard_Project_agsProjectCoordinateSystem.md).
The diagram below provides further clarification:

![Coordinate transformation diagram](images/Project_coordinatetransform.svg)

### Documents

The allocation of documents into sets
([agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) objects)
must take account of the references to sets of documents required from the relevant parts of the schema.

If a document is part of more then one set, the relevant
[agsProjectDocument](./Standard_Project_agsProjectDocument.md)
object needs to be repeated in each [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md).

If there is only one document in a set, then the
[agsProjectDocument](./Standard_Project_agsProjectDocument.md)
object must be included within an array given that the parent attribute data type is array, in accordance with
[General rules and conventions](./Standard_General_Rules.md#arrays).
Replacing the array with a single object is not permitted.

Inclusion of link(s) to the document file is recommended.

If the document is not included as part of the AGSi file/data set (with a
[URI](./Standard_General_Definitions.md)
relative link provided), then a [URI](./Standard_General_Definitions.md) link to an external resource, typically a project document management system, should be included. However, this should only be done when the intended users of the file have access to that external resource.

Files used for geometry (referenced by
[agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) objects) do not need to be included as documents.

Project requirements for documents, in particular the use of links, should be described in the
[specification](./Standard_General_Specification.md).

### Codes

The codes defined here, in [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md) and
[agsProjectCode](./Standard_Project_agsProjectCode.md), must not be used for the property and parameter codes required by [agsiDataPropertyValue](./Standard_Data_agsiDataPropertyValue.md) and
[agsiDataParameterValue](./Standard_Data_agsiDataParameterValue.md) respectively (use [agsiDataCode](./Standard_Data_agsiDataCode.md) for these).

The codes defined here are typically used for project specific
<a href="https://www.ags.org.uk/data-format/" target="_blank">ABBR codes inherited from AGS factual data</a>.
Re-use of codes from the AGS factual data, if applicable, is not mandatory. However, doing so may facilitate more  efficient data transfer.

If AGS ABBR codes are not used, it is recommended that the codes adopted should differ significantly from the AGS ABBR codes to avoid potential confusion.

For example use of `TrialPit` would be preferable to `TP`.

Inclusion of each code as individual
[agsProjectCode](./Standard_Project_agsProjectCode.md) objects is optional when the standard list of AGS ABBR codes is used, provided that use of the AGS ABBR list is identified in [agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md).

Use of the AGS ABBR lists is recommended. Use of an established alternative list, such as a project specific dictionary on a major project or an alternative published list, is permitted. In this case, inclusion of each code as an individual [agsProjectCode](./Standard_Project_agsProjectCode.md) objects is recommended. However this may be omitted if the list is identified and linked in
[agsProjectCodeSet](./Standard_Project_agsProjectCodeSet.md)
and users have access to the relevant resource.

Project requirements for codes should be described in the
[specification](./Standard_General_Specification.md).
