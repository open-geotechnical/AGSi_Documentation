# AGSi Guidance / General

## How to read UML class diagrams

Sorry. This is just a placeholder at the moment!
We'll hopefully get around to this soon.

In the meantime, check out this link, which is one of the references we used, albeit providing more detail than you probalby need.

<a href="https://developer.ibm.com/technologies/web-development/articles/the-class-diagram/" target="_blank">https://developer.ibm.com/technologies/web-development/articles/the-class-diagram/</a>
