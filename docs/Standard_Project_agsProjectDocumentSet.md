


# AGSi Standard / Project

## agsProjectDocumentSet

### Object description

Container and metadata for a set of supporting documents or reference information, which may be referenced from other parts of the schema. This container must be used and referenced, even if there is only one document within it.

The parent object of [agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) is [agsProject](./Standard_Project_agsProject.md)

[agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) contains the following embedded child objects: 

- [agsProjectDocument](./Standard_Project_agsProjectDocument.md)

[agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) has associations (reference links) with the following objects: 

- [agsProject](./Standard_Project_agsProject.md)
- [agsProjectInvestigation](./Standard_Project_agsProjectInvestigation.md)
- [agsiModel](./Standard_Model_agsiModel.md)

[agsProjectDocumentSet](./Standard_Project_agsProjectDocumentSet.md) has the following attributes:


- [documentSetID](#documentsetid)
- [description](#description)
- [agsProjectDocument](#agsprojectdocument)
- [remarks](#remarks)


### Attributes

#### documentSetID
ID used internally in this file/data set.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``GIPackageAReport``

#### description
Brief description, i.e. what this set of documents is commonly known as.  
*Type:* string  
*Example:* ``Package A factual report``

#### agsProjectDocument
Array of embedded [agsProjectDocument](./Standard_Project_agsProjectDocument.md) object(s)  
*Type:* array (agsProjectDocument object(s))  


#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some additional remarks``

