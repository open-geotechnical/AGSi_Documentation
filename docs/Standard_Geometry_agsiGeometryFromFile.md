


# AGSi Standard / Geometry

## agsiGeometryFromFile

### Object description

An [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) object is a pointer to geometry data contained within an external file, such as a CAD or model file. This object also includes metadata describing the file being referenced. Refer to [Geometry rules and conventions](./Standard_Geometry_Rules.md) for further requirements and recommendations relating to this object.

The parent object of [agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) is [agsiGeometry](./Standard_Geometry_agsiGeometry.md)

[agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) has associations (reference links) with the following objects: 

- [agsiModelElement](./Standard_Model_agsiModelElement.md)
- [agsiModelBoundary](./Standard_Model_agsiModelBoundary.md)
- [agsiGeometryVolFromSurfaces](./Standard_Geometry_agsiGeometryVolFromSurfaces.md)
- [agsiGeometryAreaFromLines](./Standard_Geometry_agsiGeometryAreaFromLines.md)

[agsiGeometryFromFile](./Standard_Geometry_agsiGeometryFromFile.md) has the following attributes:


- [geometryID](#geometryid)
- [description](#description)
- [geometryType](#geometrytype)
- [fileFormat](#fileformat)
- [fileFormatVersion](#fileformatversion)
- [fileURI](#fileuri)
- [fileLayer](#filelayer)
- [revision](#revision)
- [date](#date)
- [revisionInfo](#revisioninfo)
- [remarks](#remarks)


### Attributes

#### geometryID
Identifier that will be referenced by Model and Geometry objects as required. All identifiers used within the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``GeologyGCC``

#### description
Short description of geometry defined here  
*Type:* string  
*Example:* ``Top of GCC``

#### geometryType
Nature of geometry represented  
*Type:* string (recommend using [term from vocabulary](./Codes_Vocab.md#used-in-geometry-group))  
*Condition:* Recommended  
*Example:* ``Surface``

#### fileFormat
Format/encoding of the data, i.e. file format. Refer to vocabulary for list of common formats that may be used, or provide concise identification if other format used.  Refer to [Geometry rules and conventions](./Standard_Geometry_Rules.md) for  requirements and recommendations relating to file formats.  
*Type:* string (recommend using [term from vocabulary](./Codes_Vocab.md#used-in-geometry-group))  
*Condition:* Recommended  
*Example:* ``LandXML``

#### fileFormatVersion
Additional version information for file format used, if required.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``2.0``

#### fileURI
URI for the data file. This will be a relative link if file is included as part of the AGSi file/data set. Alternatively, a link to a location within a  project document system. Refer to [Geometry rules and conventions](./Standard_Geometry_Rules.md) for  requirements and recommendations relating to linked files.  
*Type:* string (uri)  
*Condition:* Required  
*Example:* ``geometry/geology/GCCtop.xml``

#### fileLayer
For CAD or model files, if applicable the layer/level on which the required data is located. Use with caution as the ability to interrogate only a specified layer/level may not be supported in all software.  
*Type:* string  
*Example:* ``GCCTop``

#### revision
Revision of the referenced file.  
*Type:* string  
*Condition:* Recommended  
*Example:* ``P2``

#### date
Date of issue of this revision  
*Type:* string (date)  
*Example:* ``2018-10-07``

#### revisionInfo
Revision notes for this revision of the referenced file.  
*Type:* string  
*Example:* ``Updated for GIR rev P2. Additional BH from 2018 GI included.``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``

