# AGSi Guidance / General

## Governance - Revision control

Placeholder - to be drafted

!!! Note
    Work in progress - ignore below - requires review and edit and probably some extra stuff

#### Governance

This standard has been authored by and is maintained by the *Data Management Working Group* of the
<a href="https://ags.org.uk" target="_blank">Association of Geotechnical & Geoenvironmental Specialists (AGS)</a>

!!!Note
    The AGS is a non-profit making trade association established to improve the profile and quality of geotechnical and geoenvironmental engineering. The membership comprises UK organisations and individuals having a common interest in the business of site investigation, geotechnics, geoenvironmental engineering, engineering geology, geochemistry, hydrogeology, and other related disciplines.

Revision control shall be implemented for this standard. The standard defines the schema, therefore the revision number of the standard is also the version number of the schema that it relates to.

The system used for revision numbering broadly follows the principles of
<a href="https://semver.org/" target="_blank">Sematic Versioning</a>
which adopts the *Major.Minor.Patch* convention. For AGSi this has been interpreted thus:
* *Major* versions are reserved for major changes in scope or changes that break backwards compatability
* *Minor* versions used when functionality is added in a backwards compatible manner
* *Patch* versions used for correction of errors in documentation or the schema (that do not change intent).

Further details of the system adopted for revision numbering are given in the
[Guidance on this subject](./Guidance_General_Revision.md)
