


# AGSi Standard / Geometry

## agsiGeometryColumn

### Object description

An [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md) object provides the geometric data for a single column segment, typically used for geological logging descriptions. In addition, useful information such as the geological description can be included. This object may be associated with an [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) object, but this is not essential.

The parent object of [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md) is [agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md)

[agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md) has associations (reference links) with the following objects:

- [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md)

[agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md) has the following attributes:


- [topCoordinate](#topcoordinate)
- [verticalBottomElevation](#verticalbottomelevation)
- [bottomCoordinate](#bottomcoordinate)
- [holeName](#holename)
- [holeID](#holeid)
- [description](#description)
- [legendCode](#legendcode)
- [geologyCode](#geologycode)
- [geologyCode2](#geologycode2)
- [propertySetID](#propertysetid)
- [remarks](#remarks)


### Attributes

#### topCoordinate
Co-ordinates of the top of the column segment, as a co-ordinate tuple.  
*Type:* array ([coordinate tuple](./Standard_General_Formats.md#coordinate-tuple))  
*Condition:* Required  
*Example:* ``[1275.5,2195.0,6.3]``

#### verticalBottomElevation
For a vertical column only, elevation of bottom of segment. For non vertical segments use bottomCoordinates instead.  
*Type:* number  
*Condition:* Required if bottomCoordinate not used  
*Example:* ``-28.4``

#### bottomCoordinate
Co-ordinates of the bottom of the segment, as a co-ordinate tuple. Takes precedence over verticalBottomElevation if they are in conflict.  
*Type:* array ([coordinate tuple](./Standard_General_Formats.md#coordinate-tuple))  
*Condition:* Required if verticalBottomElevation not used  
*Example:* ``[1275.5,2195.0,-28.4]``

#### holeName
Name of the relevant exploratory hole. Allows this information to be conveyed when the exploratory holes are not modelled as objects. Not required (and recommended not used) if holeID used.  
*Type:* string  
*Condition:* Recommended if holeID attribute not used  
*Example:* ``BH01``

#### holeID
Link to [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) object for the relevant exploratory hole. Reference should be made to the holeID attribute, not holeName.  
*Type:* string (reference to holeID of [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) object)  
*Condition:* Recommended if there is an [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) object for this hole  
*Example:* ``A/BH01``

#### description
Geological description, or other type of description depending on use of column  
*Type:* string  
*Example:* ``Stiff to very stiff blue grey slightly sandy silty CLAY with rare claystone layers (GOTHAM CITY CLAY)``

#### legendCode
Legend code. Recommend using code from AGS format ABBR code list.  
*Type:* string ([code based on AGS ABBR recommended](./Standard_General_Formats.md#ags-abbr-codes--project-codes))  
*Example:* ``201``

#### geologyCode
Geology code. Typically a project specific code defined as [agsProjectCodeSet](./Standard_Project_agsProjectCode.md) object.  
*Type:* string (project [code based on AGS ABBR recommended](./Standard_General_Formats.md#ags-abbr-codes--project-codes))  
*Example:* ``GCC``

#### geologyCode2
2nd geology code, if applicable. Typically a project specific code defined as [agsProjectCodeSet](./Standard_Project_agsProjectCode.md) object.  
*Type:* string (project [code based on AGS ABBR recommended](./Standard_General_Formats.md#ags-abbr-codes--project-codes))  
*Example:* ``A2``

#### propertySetID
Reference to additional property data for this column segment by reference to an [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) object.  
*Type:* string (reference to propertySetID of [agsiDataPropertySet](./Standard_Data_agsiDataPropertySet.md) object )  
*Example:* ``PropExampleRef``

#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Top of 1.2m of GCC in original log re-interpreted as ALV.``
