# AGSi Standard / Encoding

## JSON encoding

### JSON standard and version

AGSi may be encoded as JSON (Javascript Object Notation) for
file based transfer.

JSON encoding of AGSi shall comply with the following version of the JSON standard:

* <a href="https://www.iso.org/standard/71616.html" target="_blank">ISO/IEC 21778:2017 Information technology — The JSON data interchange syntax</a>

!!! Note

    Alternative versions of the JSON standard can be found at:

    * <a href="https://tools.ietf.org/html/rfc8259" target="_blank">IETF RFC8259 (December 2017)</a>
    * <a href="http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf" target="_blank">ECMA-404 (December 2017)</a>

    It is commonly reported that the above standards are consistent with each other and the ISO/IEC version.

A general introduction to JSON is provided in the
[Guidance](./Guidance_Encoding_Json.md).
A comprehensive guide can be found at
<a href="https://www.json.org/json-en.html" target="_blank">www.json.org</a>


### General requirements for AGSi encoded as JSON

AGSi objects shall be represented as JSON objects, and AGSi arrays as JSON arrays.

The AGSi schema includes nested objects. JSON encoding of AGSi shall also follow a nested approach, i.e. objects within objects, where required by the schema.

The file created shall have the extension **.json**.

### AGSi Objects

AGSi objects shall be encoded as JSON objects with the attribute names and values mapping to JSON object names and values:

    :::JSON
    { "attribute1" : "value1" , "attribute2" : "value2" }

The topmost level of the AGSi schema is the
[root schema](./Standard_Root_Intro.md)
which comprises a single object, the root object.
The entirety of an AGSi data set is contained within this single object, thus:

    :::JSON
    {  ... everything goes in this single object ... }  

Where a **child object** is embedded within a **parent object**,
the child object becomes the value for the relevant parent object attribute.

!!! Note
    AGSi objects have the namespace prefix *ags* or *agsi* to make them easier to identify

The convention adopted in AGSi naming is such that the parent attribute name is normally the child object class name:

    :::JSON
    { "agsiParentObjectName" :
        { "parentAttribute1" : "a value" ,
          "parentAttribute2" : "a value" ,
          "agsiChildObjectName" :
              { "childAttribute1" : "a value" , "childAttribute2" : "a value"}
         }
     }

### AGSi arrays

All AGSi arrays, which may comprise values, objects or other arrays, shall be encoded as JSON arrays:

    :::JSON
    { "listAttribute" : [ "this" , "is", "a", "list", "of", 7, "values" ] },

    { "agsChildObjectName" :
        [
              { "childAttributeID" : "1st child object" } ,
              { "childAttributeID" : "2nd child object" } ,
              { "childAttributeID" : "3rd child object" }
         ]
    }

Where the schema specifies an array type for an attribute,
the data must be encoded as an array, even if the data requires only one value (or object).
Replacing the array with a single value/object is not permitted.
For example:

    :::JSON
    { "arrayAttribute" : [ "array with one item is correct" ] }

    { "arrayAttribute" : "replacing array with a string is incorrect" }

Conversely, in some cases the schema asks for data
that represents a list to be input as a single text
string, not an array.
In such cases, use of an array is not permitted

!!! Note
    The schema generally requires arrays where identification
    of the separate values may be critical to correct reading, parsing or understanding of the data.

    Lists as simple text string lists are used for attributes that carry
    metadata that does not generally need to be parsed to a list of items,
    e.g. [producerSuppliers](./Standard_Project_agsProject.md#producersuppliers) in
    [agsProject](./Standard_Project_agsProject.md).

    The schema documentation makes it clear whether an array is required or not.


### Data types and format

JSON data types shall be the same as the AGSi schema
data types, which replicate the
[allowable JSON data types](./Guidance_Encoding_Json.md#json-data-types).

JSON encoding for boolean and null values is as follows (quotemarks not required):

    :::JSON
    { "this is boolean true" : true ,
      "this is boolean false" : false ,
      "and this is a null value" : null }

Numbers in JSON must comply with the JSON standard, whose requirements are similar to those used in most programming languages. Allowable number formats include:

    :::JSON
    [ 1, 0.345, -2.54, 999.99, 1.2e9, 1.2e+09, 1.2E-9]
           Not valid: [ .22, 007, +2.54 ]


Where an array is specified, the data type for each
item in the array shall be as specified in the AGSi
schema (assume string type if not specified).

Where the schema specifies additional formatting, e.g. date,
this additional formatting shall be applied to the data
in accordance with the provisions in
[Data input formats](./Standard_General_Formats.md).
Validation of data against these additional formatting
rules is possible using the
[JSON Schema for AGSi](./Standard_Encoding_JSONSchema.md)   

To comply with the JSON standard, certain characters
cannot be used in string (text) data unless escaped by
prefixing with a **\\** (backslash). This includes the following:

* Newline to be replaced with **\n**
* Carriage return to be replaced with **\r**
* Tab to be replaced with **\t**
* Double quote to be replaced with **\\"**
* Backslash to be replaced with **\\\\**

Note that **a single quote/apostrophe (') should not be escaped**.

!!! Note
    Attention is drawn to line breaks (ASCII chr(10))
    which should be encoded as **\\n**.   

### Required fields and null values

Where the AGSi schema specifies that an object and/or attribute is **required**, then this attribute must
be included in the encoded data set.
Generally this attribute will be required to have
a value (or object) defined, but in some cases  a null value or blank string value may be acceptable, as specified in the schema documentation.


For attributes that are not specified as required,
where the value of an attribute is **null or blank**, or there is no object to embed, then
the attribute name should be omitted from the data for the relevant object instance.

Use of the *null* data type for null or blank data is not permitted unless otherwise stated in the schema documentation (object reference).

For string data, use of a blank string value, i.e: ``""``, is permitted as an alternative to omission, e.g.

    :::JSON
    { "blankAttributeName" : "" }
