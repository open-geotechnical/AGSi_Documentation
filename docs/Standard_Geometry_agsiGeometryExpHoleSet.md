


# AGSi Standard / Geometry

## agsiGeometryExpHoleSet

### Object description

An [agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) object is a user defined set of exploratory holes (boreholes, trial pits, CPT etc.). A set may contain all holes, or if preferred the holes can be organised into several sets, e.g. sets for different investigations, and/or different types of hole. The data for the holes is incorporated via the embedded [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) objects. Geological data is not included in these objects: see [agsiGeometryColumnSet](./Standard_Geometry_agsiGeometryColumnSet.md) and [agsiGeometryColumn](./Standard_Geometry_agsiGeometryColumn.md).

The parent object of [agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) is [agsiGeometry](./Standard_Geometry_agsiGeometry.md)

[agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) contains the following embedded child objects: 

- [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md)

[agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) has associations (reference links) with the following objects: 

- [agsiModelElement](./Standard_Model_agsiModelElement.md)

[agsiGeometryExpHoleSet](./Standard_Geometry_agsiGeometryExpHoleSet.md) has the following attributes:


- [geometryID](#geometryid)
- [description](#description)
- [agsiGeometryExpHole](#agsigeometryexphole)
- [remarks](#remarks)


### Attributes

#### geometryID
Identifier that will be referenced by [agsiModelElement](./Standard_Model_agsiModelElement.md). All identifiers used within the [agsiGeometry](./Standard_Geometry_agsiGeometry.md) group shall be unique. Use of UUIDs for identifiers is recommended for large datasets.  
*Type:* string (identifier)  
*Condition:* Required  
*Example:* ``GIHolesA``

#### description
Short description of the set of holes defined here  
*Type:* string  
*Example:* ``2018 GI Package A``

#### agsiGeometryExpHole
Array of embedded [agsiGeometryExpHole](./Standard_Geometry_agsiGeometryExpHole.md) object(s)  
*Type:* array (agsiGeometryExpHole object(s))  


#### remarks
Additional remarks, if required  
*Type:* string  
*Example:* ``Some remarks if required``

