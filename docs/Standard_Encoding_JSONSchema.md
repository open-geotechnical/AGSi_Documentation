# AGSi Standard / Encoding

## JSON Schema

<a href="https://json-schema.org/" target="_blank">JSON Schema</a>
provides a method for describing a schema that may be used for validation. Validation tools using JSON Schema are available online.

The JSON Schema for this version of AGSi is provided in support this standard. It can be downloaded from the following:

* <a href="https://ags-data-format-wg.gitlab.io/AGSi_Documentation/schema/AGSi_JSONSchema_v-0-5-0.zip" target="_blank"> https://ags-data-format-wg.gitlab.io/AGSi_Documentation/schema/AGSi_JSONSchema_v-0-5-0.zip</a>


This JSON Schema is based on version
<a href="https://datatracker.ietf.org/doc/draft-handrews-json-schema/" target="_blank">Draft 2019-09 of the JSON Schema standard</a> (formerly known as draft-08).

In the event of any conflict between the JSON Schema file provided and the
documentation, the documentation shall take precedence.

Validation against the JSON schema will normally include the following:

* JSON syntax
* Object names and their relationships
* Attribute names
* Attribute data types
* Required objects or attributes
* Additional format rules for strings, e.g. date/time format
* Specific conditional requirements of the schema, e.g. one or other of two fields must be populated

There are some aspects that are critical to the correct formation of an AGSi file that
will **not** generally be checked by general purpose JSON Schema validation software.
These include:

* Cross-check on identifiers used for reference links between objects
* Check that supporting files are accessible
